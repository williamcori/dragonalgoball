package modelo;

import modelo.Jugador;

public class Turno {
	private static final int MAX_JUGADORES=2;
	
	private Jugador[] jugadores;
	private int cantidadJugadores;
	private int indexJugadorActual;
	
	public Turno(){
		this.cantidadJugadores=0;
		jugadores = new Jugador[MAX_JUGADORES];
	}	
	public void agregarJugador(Jugador jugador){
		jugadores[cantidadJugadores++]=jugador;
		setIndexAleatorio();
	}
	private void setIndexAleatorio(){
		indexJugadorActual= (int) (Math.random()*1000000);
		indexJugadorActual=indexJugadorActual%cantidadJugadores;	
	}
	public Jugador obtenerProximoJugador(){
		indexJugadorActual++;
		if(indexJugadorActual==cantidadJugadores)
			indexJugadorActual=0;
		return jugadores[indexJugadorActual];
	}
}
