package modelo.personaje;

import modelo.atributos.EstadoPersonaje;
import modelo.atributos.Fase;
import modelo.atributos.ataque.Ataque;
import modelo.atributos.distanciaDeAtaque.DistanciaDeAtaque;
import modelo.atributos.velocidad.Velocidad;
import modelo.excepciones.KiInsuficiente;
import modelo.transformacion.ControladorTransformacionPorKi;

public class Freezer extends Personaje {

	public Freezer() {
		super("Freezer", 400, 20);
		Fase normal = new Fase("Normal", 0, new Ataque(20), new Ataque(30), new Velocidad(4), new DistanciaDeAtaque(2));
		Fase segundaForma = new Fase("SegundaForma", 20, new Ataque(40), new Ataque(60), new Velocidad(4), new DistanciaDeAtaque(3));
		ControladorTransformacionPorKi controladorSegundaForma = new ControladorTransformacionPorKi(this);
		Fase definitivo = new Fase("Definitivo", 50, new Ataque(50), new Ataque(75), new Velocidad(6), new DistanciaDeAtaque(3));
		ControladorTransformacionPorKi controladorDefinitivo = new ControladorTransformacionPorKi(this);
		estado = new EstadoPersonaje(normal);
		estado.agregarFase(segundaForma,controladorSegundaForma);
		estado.agregarFase(definitivo,controladorDefinitivo);
	}
	
	@Override
	public void ataqueBasico(Personaje rival) {
		double poder = estado.getFase().getAtaqueNormal().getAtaque();
		atacar(rival, poder);
	}

	@Override
	public void ataqueEspecial(Personaje rival) {
		if(getKi()<costoAtaqueEspecial)
			throw new KiInsuficiente();
		disminuirKi(costoAtaqueEspecial);
		double poder = estado.getFase().getAtaqueEspecial().getAtaque();
		atacar(rival, poder);
	}

}
