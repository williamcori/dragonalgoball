package modelo.personaje;


import modelo.atributos.EstadoPersonaje;
import modelo.atributos.Fase;
import modelo.atributos.ataque.Ataque;
import modelo.atributos.distanciaDeAtaque.DistanciaDeAtaque;
import modelo.atributos.velocidad.Velocidad;
import modelo.excepciones.KiInsuficiente;
import modelo.transformacion.ControladorTransformacionPorAbsorcion;

public class Cell extends Personaje implements Absorbedor{
	
	private int cantidadAbsorciones;
	
	public Cell() {
		super("Cell", 500, 5);
		Fase normal = new Fase("Normal", 0, new Ataque(20), new Ataque(20), new Velocidad(2), new DistanciaDeAtaque(3));
		Fase semiperfecto = new Fase("SemiPerfecto", 0, new Ataque(40), new Ataque(40), new Velocidad(3), new DistanciaDeAtaque(4));
		ControladorTransformacionPorAbsorcion controladorSemiPerfecto = new ControladorTransformacionPorAbsorcion(this, 4);
		Fase perfecto = new Fase("Perfecto", 0, new Ataque(80), new Ataque(80), new Velocidad(4), new DistanciaDeAtaque(4));
		ControladorTransformacionPorAbsorcion controladorPerfecto = new ControladorTransformacionPorAbsorcion(this, 8);
		estado = new EstadoPersonaje(normal);
		estado.agregarFase(semiperfecto,controladorSemiPerfecto);
		estado.agregarFase(perfecto,controladorPerfecto);
		cantidadAbsorciones = 0;
	}

	@Override
	public void ataqueBasico(Personaje rival) {
		double poder = estado.getFase().getAtaqueNormal().getAtaque();
		atacar(rival, poder);
	}

	@Override
	public void ataqueEspecial(Personaje rival) {
		if(getKi()<costoAtaqueEspecial)
			throw new KiInsuficiente();
		disminuirKi(costoAtaqueEspecial);
		double poder = estado.getFase().getAtaqueEspecial().getAtaque();
		aumentarVida(poder);
		cantidadAbsorciones = cantidadAbsorciones + 1;
		atacar(rival, poder);
	}

	public int getCantidadAbsorciones(){
		return cantidadAbsorciones;
	}

}
