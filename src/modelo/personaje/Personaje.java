package modelo.personaje;

import modelo.atributos.EstadoPersonaje;
import modelo.atributos.ki.*;
import modelo.atributos.vida.*;
import modelo.consumibles.Consumibles;
import modelo.efectosTemporales.EfectosTemporales;
import modelo.excepciones.*;
import modelo.tablero.Posicion;
import modelo.tablero.Tablero;


public abstract class Personaje {

	protected String nombre;
	protected Vida vida;
	protected Ki ki;
	protected EstadoPersonaje estado;
	protected int costoAtaqueEspecial;
	private Tablero tablero;
	private Posicion posActual;
	private int esferas;
	private EfectosTemporales efectosTemporales;
	
	public Personaje(String nombre, double vidaInicial, int costoAtaqueEspecial) {
		this.nombre = nombre;
		this.vida = new Vida(vidaInicial);
		this.ki = new Ki();
		this.costoAtaqueEspecial=costoAtaqueEspecial;
		esferas=0;
		efectosTemporales= new EfectosTemporales();
	}
	
	public void setTablero(Tablero tablero){
		this.tablero=tablero;
	}
	
	public void consumir(Consumibles consumible){
		esferas+=consumible.sumarEsfera();
		aumentarVida(consumible.getIncrementoVida());
		efectosTemporales.consumir(consumible);
	}

	public void inmovilizar(int duracion){
		efectosTemporales.inmovilizar(duracion);
	}
	
	public boolean inmovilizado(){
		return efectosTemporales.inmovilizado();
	}
	
	public void ubicarPersonaje(Posicion pos){
		if(tablero.getPersonajeCelda(pos)==null){
			tablero.setPersonajeCelda(pos, this);
			posActual=pos;
		}
	}
	
	public void mover(Posicion posFinal){
		int velocidad = estado.getFase().getVelocidad().getVelocidad();
		velocidad=velocidad*(int)efectosTemporales.getMultiplicadorVelocidad();
		if(inmovilizado())
			throw new PersonajeInmovilizado();
		if(!tablero.movimientoValido(posActual,posFinal,velocidad))
			throw new RangoInvalido();
		tablero.moverPersonaje(this, posFinal);
	}
	
	public void haPasadoUnTurno(){
		if(estaVivo()){
			aumentarKi();
			efectosTemporales.haPasadoUnTurno();
		}
	}
	
	public void haPasadoUnAtaque(){
		efectosTemporales.haPasadoUnAtaque();
	}

	public int getCantidadEsferas(){
		return esferas;
	}
	
	public EstadoPersonaje getEstado(){
		return estado;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public double getVida() {
		return vida.getVida();
	}
	
	public double getVidaInicial(){
		return vida.getVidaInicial();
	}
	
	public int getKi() {
		return ki.getKi();
	}
	
	public int getKiMaximo(){
		return ki.getKiMaximo();
	}
	
	public Posicion getPosicion() {
		return posActual;
	}
	
	public void setPosicion(Posicion pos){
		this.posActual = pos;
	}
	
	public String getNombreFaseActual() {
		return estado.getFase().getNombre();
	}
	
	public void aumentarVida(double valor) {
		vida.aumentarVida(valor);
	}
	
	public void disminuirVida(double valor) {
		vida.disminuirVida(valor);
	}
	
	public boolean estaVivo() {
		return vida.getVida() > 0;
	}
	
	public void aumentarKi() {
		if(!inmovilizado())
			ki.aumentarKi();
	}
	
	public void disminuirKi(int valor) {
		ki.disminuirKi(valor);
	}
	
	public int getCostoTransformacion(){
		return estado.getCostoSiguienteFase();
	}
	public int getVelocidad(){
		int multiplicador=(int)efectosTemporales.getMultiplicadorVelocidad();
		return estado.getFase().getVelocidad().getVelocidad()*multiplicador;
	}
	public int getDistanciaAtaque(){
		return estado.getFase().getDistanciaDeAtaque().getDistanciaDeAtaque();
	}
	public int getCostoAtaqueEspecial(){
		return costoAtaqueEspecial;
	}
	
	public abstract void ataqueBasico(Personaje rival);
	
	public abstract void ataqueEspecial(Personaje rival);
	
	public void cambiarFase(){
		int valor = estado.getCostoSiguienteFase();
		if(inmovilizado())
			throw new PersonajeInmovilizado();
		estado.siguienteFase();
		disminuirKi(valor);
	}
	
	protected void atacar(Personaje rival, double poder) {
		int distanciaAtaque = this.estado.getFase().getDistanciaDeAtaque().getDistanciaDeAtaque();
		if(inmovilizado())
			throw new PersonajeInmovilizado();
		if(!tablero.distanciaValida(posActual, rival.posActual, distanciaAtaque))	
			throw new RangoInvalido();
		
		double poderRival = rival.estado.getFase().getAtaqueNormal().getAtaque();
		
		if (poderRival > poder)
			poder = poder * 0.8;
		poder = poder*efectosTemporales.getMultiplicadorPoder();
		rival.disminuirVida(poder);
	}
	
	public boolean equals(Personaje otroPersonaje){
		return (this.getNombre() == otroPersonaje.getNombre());
	}
}
