package modelo.personaje;

public interface Protector {
	
	public void setProtegido(Personaje personajeAProteger);
	
	public Personaje getProtegido();
	
}
