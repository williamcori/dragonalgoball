package modelo.personaje;

public interface Preocupado {
	
	public void setAmigos(Personaje[] amigos);
	
	public Personaje[] getAmigos();
	
}
