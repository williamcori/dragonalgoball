package modelo.personaje;

import modelo.atributos.EstadoPersonaje;
import modelo.atributos.Fase;
import modelo.atributos.ataque.Ataque;
import modelo.atributos.distanciaDeAtaque.DistanciaDeAtaque;
import modelo.atributos.velocidad.Velocidad;
import modelo.excepciones.KiInsuficiente;
import modelo.transformacion.ControladorTransformacionPorKi;
import modelo.transformacion.ControladorTransformacionPorProteccion;

public class Piccolo extends Personaje implements Protector {
	private Personaje protegido;
	
	public Piccolo() {
		super("Piccolo", 500, 10);
		Fase normal = new Fase("Normal", 0, new Ataque(20), new Ataque(25), new Velocidad(2), new DistanciaDeAtaque(2));
		Fase fortalecido = new Fase("Fortalecido", 20, new Ataque(40), new Ataque(50), new Velocidad(3), new DistanciaDeAtaque(4));
		ControladorTransformacionPorKi controladorFortalecido = new ControladorTransformacionPorKi(this);
		Fase protector = new Fase("Protector", 0, new Ataque(60), new Ataque(75), new Velocidad(4), new DistanciaDeAtaque(6));
		ControladorTransformacionPorProteccion controladorProtector = new ControladorTransformacionPorProteccion(this, 0.2);
		estado = new EstadoPersonaje(normal);
		estado.agregarFase(fortalecido,controladorFortalecido);
		estado.agregarFase(protector,controladorProtector);
	}

	@Override
	public void ataqueBasico(Personaje rival) {
		double poder = estado.getFase().getAtaqueNormal().getAtaque();
		atacar(rival, poder);
	}

	@Override
	public void ataqueEspecial(Personaje rival) {
		if(getKi()<costoAtaqueEspecial)
			throw new KiInsuficiente();
		disminuirKi(costoAtaqueEspecial);
		double poder = estado.getFase().getAtaqueEspecial().getAtaque();
		atacar(rival, poder);
	}

	@Override
	public void setProtegido(Personaje personajeAProteger) {
		protegido = personajeAProteger;
		
	}

	@Override
	public Personaje getProtegido() {
		return protegido;
	}

}
