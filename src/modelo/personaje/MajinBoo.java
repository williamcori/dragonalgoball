package modelo.personaje;

import modelo.atributos.EstadoPersonaje;
import modelo.atributos.Fase;
import modelo.atributos.ataque.Ataque;
import modelo.atributos.distanciaDeAtaque.DistanciaDeAtaque;
import modelo.atributos.velocidad.Velocidad;
import modelo.excepciones.KiInsuficiente;
import modelo.transformacion.ControladorTransformacionPorKi;

public class MajinBoo extends Personaje {
	
	private static final int DURACION_DE_EFECTO_CONVIERTETE_EN_CHOCOLATE=3;

	public MajinBoo() {
		super("MajinBoo", 300, 30);
		Fase normal = new Fase("Normal", 0, new Ataque(30), new Ataque(0), new Velocidad(2), new DistanciaDeAtaque(2));
		Fase malo = new Fase("Malo", 20, new Ataque(50), new Ataque(0), new Velocidad(3), new DistanciaDeAtaque(2));
		ControladorTransformacionPorKi controladorMalo = new ControladorTransformacionPorKi(this);
		Fase original = new Fase("Original", 50, new Ataque(60), new Ataque(0), new Velocidad(4), new DistanciaDeAtaque(3));
		ControladorTransformacionPorKi controladorOriginal = new ControladorTransformacionPorKi(this);
		estado = new EstadoPersonaje(normal);
		estado.agregarFase(malo,controladorMalo);
		estado.agregarFase(original,controladorOriginal);
	}
	
	@Override
	public void ataqueBasico(Personaje rival) {
		double poder = estado.getFase().getAtaqueNormal().getAtaque();
		atacar(rival, poder);
	}

	@Override
	public void ataqueEspecial(Personaje rival) {
		if(getKi()<costoAtaqueEspecial)
			throw new KiInsuficiente();
		rival.inmovilizar(DURACION_DE_EFECTO_CONVIERTETE_EN_CHOCOLATE);
		disminuirKi(costoAtaqueEspecial);
	}
}
