package modelo.personaje;

import modelo.atributos.EstadoPersonaje;
import modelo.atributos.Fase;
import modelo.atributos.ataque.Ataque;
import modelo.atributos.distanciaDeAtaque.DistanciaDeAtaque;
import modelo.atributos.velocidad.Velocidad;
import modelo.excepciones.KiInsuficiente;
import modelo.transformacion.ControladorTransformacionPorKi;

public class Goku extends Personaje {
	
	public Goku() {
		super("Goku", 500, 20);
		Fase normal = new Fase("Normal", 0, new Ataque(20), new Ataque(30), new Velocidad(2), new DistanciaDeAtaque(2));
		Fase kaioken = new Fase("Kaioken", 20, new Ataque(40), new Ataque(60), new Velocidad(3), new DistanciaDeAtaque(4));
		ControladorTransformacionPorKi controladorKaioken = new ControladorTransformacionPorKi(this);
		Fase ssj = new Fase("SS", 50, new Ataque(60), new Ataque(90), new Velocidad(5), new DistanciaDeAtaque(4));
		ControladorTransformacionPorKi controladorssj = new ControladorTransformacionPorKi(this);
		estado = new EstadoPersonaje(normal);
		estado.agregarFase(kaioken, controladorKaioken);
		estado.agregarFase(ssj,controladorssj);
	}
	
	@Override
	public void ataqueBasico(Personaje rival) {
		double poder = estado.getFase().getAtaqueNormal().getAtaque();
		
		if (vida.getVida() < 150)
			poder = poder * 1.2;
		
		atacar(rival, poder);
	}

	@Override
	public void ataqueEspecial(Personaje rival) {
		if(getKi()<costoAtaqueEspecial)
			throw new KiInsuficiente();
		disminuirKi(costoAtaqueEspecial);
		double poder = estado.getFase().getAtaqueEspecial().getAtaque();
		
		if (vida.getVida() < 150)
			poder = poder * 1.2;
		
		atacar(rival, poder);
	}


}
