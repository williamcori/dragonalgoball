package modelo.personaje;

import modelo.atributos.EstadoPersonaje;
import modelo.atributos.Fase;
import modelo.atributos.ataque.Ataque;
import modelo.atributos.distanciaDeAtaque.DistanciaDeAtaque;
import modelo.atributos.velocidad.Velocidad;
import modelo.excepciones.KiInsuficiente;
import modelo.transformacion.ControladorTransformacionPorEnojo;
import modelo.transformacion.ControladorTransformacionPorKi;

public class Gohan extends Personaje implements Preocupado {

	private Personaje[] amigos;

	public Gohan() {
		super("Gohan", 300, 10);
		Fase normal = new Fase("Normal", 0, new Ataque(15), new Ataque(18.75), new Velocidad(2), new DistanciaDeAtaque(2));
		Fase ssj1 = new Fase("SS1", 10, new Ataque(30), new Ataque(37.5), new Velocidad(2), new DistanciaDeAtaque(2));
		ControladorTransformacionPorKi controladorSsj1 = new ControladorTransformacionPorKi(this);
		Fase ssj2 = new Fase("SS2", 30, new Ataque(100), new Ataque(125), new Velocidad(3), new DistanciaDeAtaque(4));
		ControladorTransformacionPorEnojo controladorSsj2 = new ControladorTransformacionPorEnojo(this, 0.3);
		estado = new EstadoPersonaje(normal);
		estado.agregarFase(ssj1,controladorSsj1);
		estado.agregarFase(ssj2,controladorSsj2);
	}
	
	@Override
	public void ataqueBasico(Personaje rival) {
		double poder = estado.getFase().getAtaqueNormal().getAtaque();
		atacar(rival, poder);
	}

	@Override
	public void ataqueEspecial(Personaje rival) {
		if(getKi()<costoAtaqueEspecial)
			throw new KiInsuficiente();
		disminuirKi(costoAtaqueEspecial);
		double poder = estado.getFase().getAtaqueEspecial().getAtaque();
		atacar(rival, poder);
	}

	@Override
	public void setAmigos(Personaje[] amigos) {
		this.amigos = amigos;
		
	}

	@Override
	public Personaje[] getAmigos() {
		return amigos;
	}

}
