
package modelo;

import modelo.personaje.*;
import modelo.tablero.Posicion;
import modelo.tablero.Tablero;

public class Jugador {
	
	private static final int CANTIDAD_PERSONAJES=3;
	
	private Personaje[] personajes;
	private int cantidadPersonajes;
	private Tablero tablero;
	private Jugada jugada;
	private Personaje ultimoUtilizado;
	private String nombreEquipo;

	public Jugador(Tablero tablero){
		this.tablero=tablero;
		cantidadPersonajes=0;
		personajes=new Personaje[CANTIDAD_PERSONAJES];
	}
	public void jugar(){
		for(Personaje personaje:personajes){
			if(!personaje.estaVivo()){
				tablero.setPersonajeCelda(personaje.getPosicion(), null);
				personaje.disminuirKi(personaje.getKi());
			}else if(!ultimoUtilizado.estaVivo())
				ultimoUtilizado=personaje;
			personaje.haPasadoUnTurno();
		}		
		jugada =new Jugada(ultimoUtilizado);
	}
	public void accionarConPosicion(Posicion pos){
		jugada.accionarConPosicion(pos);
		if(tablero.getConsumibleCelda(pos)!=null){
			jugada.consumir(tablero.retirarConsumibleCelda(pos));
		}
	}
	public void transformarPersonajeSeleccionado(){
		jugada.transformarPersonajeSeleccionado();
	}
	public void accionarConPersonaje(Personaje personaje){
		jugada.accionarConPersonaje(personaje, mePertenece(personaje));
	}
	public void activarAtaqueEspecial(){
		jugada.activarAtaqueEspecial();
	}
	public void desactivarAtaqueEspecial(){
		jugada.desactivarAtaqueEspecial();
	}
	public void pasarTurno(){
		actualizarUltimoUtilizado();
		jugada.finalizarJugada();
	}
	public boolean terminoSuTurno(){
		actualizarUltimoUtilizado();
		return jugada.jugadaFinalizada();
	}
	private void actualizarUltimoUtilizado(){
		ultimoUtilizado=jugada.getPersonajeSeleccionado();
	}
	private boolean mePertenece(Personaje personaje){
		for(Personaje each: personajes){
			if(each==personaje)
				return true;
		}
		return false;
	}
	public boolean derrotado(){
		for(Personaje personaje: personajes){
			if(personaje.estaVivo()){
				return false;
			}
		}
		return true;
	}
	public Personaje[] getPersonajes(){
		return personajes;
	}
	public boolean completaEsferas(){
		int cantidadEsferas=0;
		for(Personaje personaje:personajes)
			cantidadEsferas+=personaje.getCantidadEsferas();
		if(cantidadEsferas==7)
			return true;
		return false;
	}
	public void establecerGuerrerosZ(){
		nombreEquipo = "Guerreros Z";
		Personaje goku = new Goku();
		Personaje gohan = new Gohan();
		Personaje piccolo = new Piccolo();
		agregarPersonaje(goku);
		agregarPersonaje(gohan);
		agregarPersonaje(piccolo);
		ultimoUtilizado=personajes[0];
		((Protector) piccolo).setProtegido(gohan);
		Personaje[] amigos = {goku,piccolo};
		((Preocupado) gohan).setAmigos(amigos);		
	}
	public void establecerEnemigosDeLaTierra(){
		nombreEquipo = "Enemigos de la tierra";
		agregarPersonaje(new Cell());
		agregarPersonaje(new Freezer());
		agregarPersonaje(new MajinBoo());	
		ultimoUtilizado=personajes[0];
	}
	
	private void agregarPersonaje(Personaje personaje){
		personajes[cantidadPersonajes]=personaje;
		personajes[cantidadPersonajes++].setTablero(tablero);
	}
	
	public void ubicarPersonajesEnExtremoSuperiorDerecho(int ancho, int alto){
		Posicion pos;
		for(int i=ancho;i!=ancho-cantidadPersonajes;i--){
			pos=new Posicion(i-1,0);
			personajes[ancho-i].ubicarPersonaje(pos);
		}
	}
	public void ubicarPersonajesEnExtremoSuperiorIzquierdo(){
		Posicion pos;
		for(int i=0;i!=cantidadPersonajes;i++){
			pos=new Posicion(i,0);
			personajes[i].ubicarPersonaje(pos);
		}
	}
	public void ubicarPersonajesEnExtremoInferiorDerecho(int ancho, int alto){
		Posicion pos;
		for(int i=ancho;i!=ancho-cantidadPersonajes;i--){
			pos=new Posicion(i-1,alto-1);
			personajes[ancho-i].ubicarPersonaje(pos);
		}
	}
	public void ubicarPersonajesEnExtremoInferiorIzquierdo(int ancho, int alto){
		Posicion pos;
		for(int i=0;i!=cantidadPersonajes;i++){
			pos=new Posicion(i,alto-1);
			personajes[i].ubicarPersonaje(pos);
		}
	}
	
	public String getNombreEquipo(){
		return nombreEquipo;
	}
	public Personaje getPersonajeSeleccionado(){
		return jugada.getPersonajeSeleccionado();
	}
}
