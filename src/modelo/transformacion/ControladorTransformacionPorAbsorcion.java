package modelo.transformacion;

import modelo.excepciones.AbsorcionesInsuficientes;
import modelo.personaje.Absorbedor;

public class ControladorTransformacionPorAbsorcion extends ControladorTransformacion {
	Absorbedor personaje;
	int absorcionesNecesarias;
	
	public ControladorTransformacionPorAbsorcion(Absorbedor personaje, int cantidadAbsorcionesNecesarias) {
		this.personaje = personaje;
		this.absorcionesNecesarias = cantidadAbsorcionesNecesarias;
	}
	
	@Override
	public void esPosibleTransformarse() {
		if(personaje.getCantidadAbsorciones() < absorcionesNecesarias)
			throw new AbsorcionesInsuficientes();
	}

}
