package modelo.transformacion;

import modelo.excepciones.ProtegidoFueraDePeligro;
import modelo.personaje.Protector;

public class ControladorTransformacionPorProteccion extends ControladorTransformacion {
	private Protector protector;
	private double multiplicadorVidaMinima;
	
	public ControladorTransformacionPorProteccion(Protector protector, double multiplicadorVidaMinima) {
		this.protector = protector;
		this.multiplicadorVidaMinima =  multiplicadorVidaMinima;
	}
	
	@Override
	public void esPosibleTransformarse() {
		if(protector.getProtegido().getVida() > multiplicadorVidaMinima*protector.getProtegido().getVidaInicial())
			throw new ProtegidoFueraDePeligro();
	}

}
