package modelo.transformacion;

import modelo.excepciones.AmigosFueraDePeligro;
import modelo.personaje.Personaje;
import modelo.personaje.Preocupado;

public class ControladorTransformacionPorEnojo extends ControladorTransformacion {
	/*Se activa si todos los amigos estan por debajo de cierto porcentaje de vida*/
	
	private Preocupado personaje;
	private double multiplicador;
	
	public ControladorTransformacionPorEnojo(Preocupado personaje, double multiplicadorVidaMinima) {
		this.personaje = personaje;
		this.multiplicador = multiplicadorVidaMinima;
	}
	
	@Override
	public void esPosibleTransformarse() {
		for(Personaje amigo:personaje.getAmigos()){
			if(amigo.getVida() > (multiplicador*amigo.getVidaInicial()))
				throw new AmigosFueraDePeligro();
		}
	}

}
