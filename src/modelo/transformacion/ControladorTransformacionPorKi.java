package modelo.transformacion;

import modelo.excepciones.KiInsuficiente;
import modelo.personaje.Personaje;

public class ControladorTransformacionPorKi extends ControladorTransformacion {
	private Personaje personaje;
	
	public ControladorTransformacionPorKi(Personaje personajeAControlar) {
		this.personaje = personajeAControlar;
	}
	
	@Override
	public void esPosibleTransformarse() {
		if(personaje.getKi() < personaje.getCostoTransformacion())
			throw new KiInsuficiente();
	}

}
