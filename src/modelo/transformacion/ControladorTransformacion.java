package modelo.transformacion;


public abstract class ControladorTransformacion {
	
	public abstract void esPosibleTransformarse();
	
}
