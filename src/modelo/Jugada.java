
package modelo;

import modelo.consumibles.Consumibles;
import modelo.excepciones.*;
import modelo.personaje.Personaje;
import modelo.tablero.Posicion;

public class Jugada {
	
	private Personaje personajeSeleccionado;
	private boolean ataqueRealizado;
	private boolean movimientoRealizado;
	private boolean ataqueEspecialActivado;
	
	public Jugada(Personaje personaje){
		seleccionarPersonaje(personaje);
		ataqueRealizado=false;
		movimientoRealizado=false;
		ataqueEspecialActivado=false;
	}
	public void accionarConPosicion(Posicion pos){
		if(movimientoRealizado)
			throw new MovimientoYaRealizado();
		getPersonajeSeleccionado().mover(pos);
		marcarMovimiento();
	}
	public void transformarPersonajeSeleccionado(){
		personajeSeleccionado.cambiarFase();
	}
	public void accionarConPersonaje(Personaje personaje, boolean personajeEsMio){
		if(personajeEsMio){
			seleccionarPersonaje(personaje);
			return;
		}
		if(ataqueRealizado)
			throw new AtaqueYaRealizado();
		if(ataqueEspecialActivado){
			personajeSeleccionado.ataqueEspecial(personaje);
		} else {
			personajeSeleccionado.ataqueBasico(personaje);
		}
		marcarAtaque();		
		personajeSeleccionado.haPasadoUnAtaque();
	}
	public boolean ataqueEspecialActivado(){
		return ataqueEspecialActivado;
	}
	public void activarAtaqueEspecial(){
		ataqueEspecialActivado=true;
	}
	public void desactivarAtaqueEspecial(){
		ataqueEspecialActivado=false;
	}
	public Personaje getPersonajeSeleccionado(){
		return personajeSeleccionado;
	}
	public boolean jugadaFinalizada(){
		return ataqueRealizado && movimientoRealizado;
	}
	public void seleccionarPersonaje(Personaje personaje){
		personajeSeleccionado=personaje;
	}
	private void marcarAtaque(){
		ataqueRealizado=true;
	}
	private void marcarMovimiento(){
		movimientoRealizado=true;
	}
	public void finalizarJugada(){
		marcarAtaque();
		marcarMovimiento();
	}
	public void consumir(Consumibles consumible) {
		personajeSeleccionado.consumir(consumible);
	}
}
