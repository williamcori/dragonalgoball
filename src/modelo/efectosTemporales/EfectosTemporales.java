package modelo.efectosTemporales;

import modelo.consumibles.Consumibles;

public class EfectosTemporales {
	
	private int duracionRestanteEfectoVelocidad;
	private int duracionRestanteEfectoPoder;
	private int duracionRestanteEfectoInmovilizacion;
	private double multiplicadorVelocidad;
	private double multiplicadorPoder;
	
	public EfectosTemporales(){
		duracionRestanteEfectoVelocidad=0;
		duracionRestanteEfectoPoder=0;
		duracionRestanteEfectoInmovilizacion=0;
	}
	public void consumir(Consumibles consumible){
		setMultiplicadorPoder(consumible.getMultiplicadorPoder());
		setMultiplicadorVelocidad(consumible.getMultiplicadorVelocidad());
		incrementarDuracionVelocidad(consumible.getDuracionDeEfectoVelocidad());		
		incrementarDuracionPoder(consumible.getDuracionDeEfectoPoder());
	}
	public void haPasadoUnTurno(){
		decrementarDuracionVelocidad();
		decrementarDuracionInmovilizacion();
	}
	public void haPasadoUnAtaque(){
		decrementarDuracionPoder();
	}
	public void inmovilizar(int duracion){
		incrementarDuracionInmovilizacion(duracion);
	}
	private void setMultiplicadorVelocidad(double multiplicador){
		multiplicadorVelocidad=multiplicador;
	}
	public double getMultiplicadorVelocidad(){
		if(duracionRestanteEfectoVelocidad>0)
			return multiplicadorVelocidad;
		return 1;
	}
	private void setMultiplicadorPoder(double multiplicador){
		multiplicadorPoder=multiplicador;
	}
	public double getMultiplicadorPoder(){
		if(duracionRestanteEfectoPoder>0)
			return multiplicadorPoder;
		return 1;
	}
	public boolean inmovilizado(){
		return duracionRestanteEfectoInmovilizacion>0;
	}
	private void incrementarDuracionVelocidad(int duracion){
		duracionRestanteEfectoVelocidad+=duracion;
	}
	private void incrementarDuracionPoder(int duracion){
		duracionRestanteEfectoPoder+=duracion;
	}
	private void incrementarDuracionInmovilizacion(int duracion){
		duracionRestanteEfectoInmovilizacion+=duracion;
	}
	private void decrementarDuracionPoder(){
		if(duracionRestanteEfectoPoder>0)
			duracionRestanteEfectoPoder--;
	}
	private void decrementarDuracionVelocidad(){
		if(duracionRestanteEfectoVelocidad>0)
			duracionRestanteEfectoVelocidad--;
	}
	private void decrementarDuracionInmovilizacion(){
		if(duracionRestanteEfectoInmovilizacion>0)
			duracionRestanteEfectoInmovilizacion--;
	}
}
