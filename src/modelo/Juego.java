package modelo;

import modelo.Turno;
import modelo.consumibles.AgregaConsumibles;
import modelo.tablero.Tablero;
import modelo.Jugador;

public class Juego {
	private static final int ANCHO=10;
	private static final int ALTO=7;
	private static final int CANTIDAD_JUGADORES=2;
	
	private Tablero tablero;
	private Turno turno;
	private Jugador[] jugadores;
	private Jugador jugadorActual;
	private AgregaConsumibles agregaConsumibles;

	
	public Juego(){
		tablero=new Tablero(ANCHO, ALTO);
		turno=new Turno();
		jugadores = new Jugador[CANTIDAD_JUGADORES];
		agregaConsumibles= new AgregaConsumibles(tablero);
		establecerJugadores();
		establecerEquipos();
		ubicarPersonajes();
	}
	private void establecerJugadores(){
		for(int i=0;i<CANTIDAD_JUGADORES;i++){
			jugadores[i]= new Jugador(tablero);
			turno.agregarJugador(jugadores[i]);
		}
	}
	private void establecerEquipos(){
		jugadores[0].establecerGuerrerosZ();
		jugadores[1].establecerEnemigosDeLaTierra();
	}
	private void ubicarPersonajes(){
		jugadores[0].ubicarPersonajesEnExtremoSuperiorIzquierdo();
		jugadores[1].ubicarPersonajesEnExtremoInferiorDerecho(ANCHO, ALTO);
	}
	public void jugar(){
			jugadorActual=turno.obtenerProximoJugador();
			jugadorActual.jugar();
			agregaConsumibles.decidirAlAzarSiAgregarConsumible();
	}
	public Jugador obtenerGanador(){
		for(int i=0;i!=CANTIDAD_JUGADORES;i++){
			if(jugadores[i].completaEsferas())
				return jugadores[i];
		}
		for(int i=0;i!=CANTIDAD_JUGADORES;i++){
			if(!jugadores[i].derrotado())
				return jugadores[i];
		}
		return null;
	}
	
	public Jugador getJugadorActual(){
		return jugadorActual;
	}
	
	public boolean juegoTerminado(){
		for(int i=0;i!=CANTIDAD_JUGADORES;i++){
			if(jugadores[i].completaEsferas())
				return true;
		}
		int cantidadJugadoresDerrotados=0;
		for(int i=0;i!=CANTIDAD_JUGADORES;i++){
			if(jugadores[i].derrotado())
				cantidadJugadoresDerrotados++;
		}
		return cantidadJugadoresDerrotados==CANTIDAD_JUGADORES-1;
	}
	public Tablero getTablero(){
		return tablero;
	}
	public Jugador[] getJugadores() {
		return jugadores;
	}
}
