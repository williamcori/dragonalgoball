package modelo.atributos.velocidad;

import modelo.excepciones.VelocidadInvalida;

public class Velocidad {
	private int velocidad;
	
	public Velocidad(int velocidadInicial){
		if(velocidadInicial <= 0){
			throw new VelocidadInvalida();
		}
		this.velocidad = velocidadInicial;
	}
	
	public int getVelocidad(){
		return this.velocidad;
	}
	
	public void activarDobleVelocidad(){
		this.velocidad = this.velocidad*2;
	}
	
	public void desactivarDobleVelocidad(){
		this.velocidad = this.velocidad/2;
	}
}
