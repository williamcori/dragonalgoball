package modelo.atributos;

import modelo.atributos.ataque.Ataque;
import modelo.atributos.distanciaDeAtaque.DistanciaDeAtaque;
import modelo.atributos.velocidad.Velocidad;

/*Fase*/
public class Fase {
	private String nombreFase;
	private Ataque ataqueNormal,ataqueEspecial;
	private Velocidad velocidad;
	private DistanciaDeAtaque distanciaDeAtaque;
	private int costoFase;
	
	public Fase(String nombreFase, int costoFase, Ataque ataque, Ataque ataqueEspecial, Velocidad velocidad, DistanciaDeAtaque distanciaDeAtaque) {
		this.nombreFase = nombreFase;
		this.costoFase = costoFase;
		this.ataqueNormal = ataque;
		this.ataqueEspecial = ataqueEspecial;
		this.velocidad = velocidad;
		this.distanciaDeAtaque = distanciaDeAtaque;
	}
	
	public String getNombre(){
		return nombreFase;
	}
	
	public Ataque getAtaqueNormal(){
		return ataqueNormal;
	}
	
	public Ataque getAtaqueEspecial(){
		return ataqueEspecial;
	}
	
	public Velocidad getVelocidad(){
		return velocidad;
	}
	
	public DistanciaDeAtaque getDistanciaDeAtaque(){
		return distanciaDeAtaque;
	}
	
	public int getCostoFase(){
		return costoFase;
	}
}
