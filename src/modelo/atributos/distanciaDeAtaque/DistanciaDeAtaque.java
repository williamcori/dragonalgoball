package modelo.atributos.distanciaDeAtaque;

public class DistanciaDeAtaque {
	private int distanciaDeAtaque;
	
	public DistanciaDeAtaque(int distanciaDeAtaque){
		this.distanciaDeAtaque = distanciaDeAtaque;
	}
	
	public int getDistanciaDeAtaque(){
		return distanciaDeAtaque;
	}
	
}
