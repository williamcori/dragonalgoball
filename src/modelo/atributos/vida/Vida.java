package modelo.atributos.vida;

import modelo.excepciones.ValorInvalido;

public class Vida {
	private double vidaActual;
	private double vidaInicial;
	
	public Vida(double vidaInicial){
		this.vidaActual = vidaInicial;
		this.vidaInicial = vidaInicial;
	}
	
	public double getVida(){
		return vidaActual;
	}
	
	public double getVidaInicial(){
		return this.vidaInicial;
	}
	
	public void disminuirVida(double damage){
		if(damage <= 0)
			throw new ValorInvalido();
		vidaActual = vidaActual - damage;
		if(vidaActual < 0)
			vidaActual = 0;
	}
	
	public void aumentarVida(double aumento){
		if(aumento < 0){
			throw new ValorInvalido();
		}
		vidaActual += aumento;
		if(this.vidaActual > this.vidaInicial)
			this.vidaActual = this.vidaInicial;
	}
}