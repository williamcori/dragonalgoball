package modelo.atributos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import modelo.excepciones.*;
import modelo.transformacion.ControladorTransformacion;

public class EstadoPersonaje {
	private ArrayList<Fase> fases;
	private Fase faseActual,faseFinal;
	private int indice;
	private Map<Fase,ControladorTransformacion> controladores;
	
	public EstadoPersonaje(Fase faseInicial){
		fases = new ArrayList<>();
		controladores = new HashMap<>();
		fases.add(faseInicial);
		faseActual = faseInicial;
		faseFinal = faseInicial;
		indice = 0;
	}
	
	public void agregarFase(Fase fase, ControladorTransformacion controladorDeEstaFase){
		fases.add(fase);
		controladores.put(fase,controladorDeEstaFase);
		faseFinal = fase;
	}
	
	public void siguienteFase(){
		if(esFaseFinal())
			throw new PersonajeEnUltimaFase();
		controladores.get(fases.get(indice+1)).esPosibleTransformarse();
		indice++;
		faseActual = fases.get(indice);
	}
	
	public Fase getFase(){
		return faseActual;
	}
	
	public int getCostoSiguienteFase(){
		if (esFaseFinal()) {
			return 0;
		}
		return fases.get(indice+1).getCostoFase();
	}
	
	private boolean esFaseFinal(){
		return faseActual.equals(faseFinal);
	}
}
