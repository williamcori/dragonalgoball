package modelo.atributos.ki;

import modelo.excepciones.KiInsuficiente;

public class Ki {
	final int VALOR_INICIAL = 0, AUMENTO_POR_TURNO = 5, VALOR_MAXIMO = 300;
	int ki;
	
	public Ki(){
		ki = VALOR_INICIAL;
	}
	
	public int getKi(){
		return ki;
	}
	
	public void aumentarKi(){
		ki = ki + AUMENTO_POR_TURNO;
		if( ki > VALOR_MAXIMO)
			ki = VALOR_MAXIMO;
	}
	
	public void disminuirKi(int valorADisminuir){
		if(valorADisminuir > ki) {
			throw new KiInsuficiente();
		}
		ki = ki - valorADisminuir;
	}
	
	public int getKiMaximo(){
		return VALOR_MAXIMO;
	}
}
