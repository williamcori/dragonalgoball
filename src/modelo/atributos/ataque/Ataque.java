package modelo.atributos.ataque;

public class Ataque {
	private double ataque, modificador;
	
	public Ataque(double ataque){
		this.ataque = ataque;
		this.modificador = 1;
	}
	
	public double getAtaque(){
		return ataque*modificador;
	}
	
	public void setModificador(double modificador){
		this.modificador = modificador;
	}
	
	public void resetModificador(){
		this.modificador = 1;
	}
}
