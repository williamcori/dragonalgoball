package modelo.tablero;

import modelo.consumibles.Consumibles;
import modelo.personaje.Personaje;

public class Celda {
	
	private Personaje personaje;
	private Consumibles consumible;
	
	public Celda(){
		this.personaje = null;
		this.consumible=null;
	}

	public Personaje getPersonaje() {
		return personaje;
	}

	public void setPersonaje(Personaje personaje) {
		this.personaje = personaje;
	}
	
	public boolean estaVacia(){
		return personaje == null && consumible==null;
	}
	
	public boolean hayPersonaje(){
		return this.personaje != null;
	}

	public void setConsumible(Consumibles consumible) {
		this.consumible=consumible;
	}
	public Consumibles getConsumible(){
		return consumible;
	}
	
	public Consumibles retirarConsumible(){
		Consumibles consumible = this.consumible;
		this.consumible = null;
		return consumible;
	}
}
