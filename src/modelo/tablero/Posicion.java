package modelo.tablero;

public class Posicion {

	
	private int x;
	private int y;
	
	public Posicion(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	
	@Override
	public boolean equals(Object obj){
	
		if (obj != null && obj.getClass() == this.getClass()){
			Posicion p = (Posicion)obj;
			return (this.x == p.x) && (this.y == p.y); 
		}
		return false;
	}
	
	public int distancia(Posicion p){
		
		int distanciaX = Math.abs(this.x - p.x);
		int distanciaY = Math.abs(this.y - p.y);
		
		return Math.max(distanciaX, distanciaY);
	}
	
	public Posicion[] posicionesColindantes(){
		Posicion[] posicionesColindantes = new Posicion[8];
		int posX = this.x - 1;
		for (int i = 0; i <3; i++){
			posicionesColindantes[i] = new Posicion(posX, this.y + 1);
			posicionesColindantes[i+3] = new Posicion(posX,this.y -1);
			posX++;
		}
		posicionesColindantes[6] = new Posicion(this.x + 1, this.y);
		posicionesColindantes[7] = new Posicion(this.x - 1, this.y);
		
		return posicionesColindantes;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public boolean esNegativa(){
		return (this.x < 0) || (this.y < 0);
	}
	

	public boolean menorQue(Posicion p){
		return this.x < p.x && this.y < p.y;
	}


	@Override
	public String toString() {
		return "(" + this.x + ", " + this.y + ")";
	}
	
	
}
