package modelo.tablero;

import java.util.ArrayList;
import java.util.List;

import modelo.consumibles.Consumibles;
import modelo.personaje.Personaje;


public class Tablero {
	/*
	 * Es un tablero rectangular que va desde (0,0)
	 * hasta los limites positivos que se designen al crearlo.
	 * 
	 * 
	 */
	private int limiteX;
	private int limiteY;
	private Celda[][] celdas;
	
	
	public Tablero(int limiteX, int limiteY){
		this.limiteX = limiteX;
		this.limiteY = limiteY;
		this.celdas = new Celda[limiteX][limiteY];
		
		for (int i = 0; i < limiteX; i++){
			for (int j = 0; j < limiteY; j++){
				this.celdas[i][j] = new Celda();
			}
		}
	}
	
	
	public boolean posicionValida(Posicion pos){
		return !pos.esNegativa() && (pos.menorQue(new Posicion(limiteX, limiteY)));
	}
	
	
	public boolean celdaVacia(Posicion pos){
		return this.celdas[pos.getX()][pos.getY()].estaVacia();
	}
	public boolean hayPersonajeEn(Posicion pos){
		return celdas[pos.getX()][pos.getY()].hayPersonaje();
	}
	
	private List<Posicion> movimientosIntermedios(Posicion pInicial, Posicion pFinal, int movDisponibles){
		
		Posicion[] posColindantes = pInicial.posicionesColindantes();
		List<Posicion> movimientosPosibles = new ArrayList<Posicion>();
		
		for (Posicion pos : posColindantes){
			if (this.posicionValida(pos) && (pos.distancia(pFinal)<= movDisponibles - 1)){
				if (!this.hayPersonajeEn(pos) || pos.equals(pFinal))
					movimientosPosibles.add(pos);
			}
		}
		
		return movimientosPosibles;
	}
	
	
	public boolean distanciaValida(Posicion pInicial, Posicion pFinal, int movDisponibles){
		
		List<Posicion> movimientosPosibles = this.movimientosIntermedios(pInicial, pFinal, movDisponibles);
		
		if (movimientosPosibles.contains(pFinal)) 
			return true;
		
		for (Posicion pos : movimientosPosibles){
			if (this.distanciaValida(pos, pFinal, movDisponibles - 1))
				return true;
		}
		
		return false;
	}
	
	
	public boolean movimientoValido(Posicion pInicial, Posicion pFinal, int movDisponibles){
		return (getPersonajeCelda(pFinal)==null && distanciaValida(pInicial, pFinal, movDisponibles));
	}
	
	public void setPersonajeCelda(Posicion pos, Personaje personaje){
		Celda celda = this.celdas[pos.getX()][pos.getY()];
		celda.setPersonaje(personaje);
	}
	
	public Personaje getPersonajeCelda(Posicion pos){
		return celdas[pos.getX()][pos.getY()].getPersonaje();
	}
	public void setConsumibleCelda(Posicion pos, Consumibles consumible){
		Celda celda= celdas[pos.getX()][pos.getY()];
		celda.setConsumible(consumible);
	}
	public Consumibles getConsumibleCelda(Posicion pos){
		return celdas[pos.getX()][pos.getY()].getConsumible();
	}
	
	public Consumibles retirarConsumibleCelda(Posicion pos){
		return celdas[pos.getX()][pos.getY()].retirarConsumible();
	}
	public int getAncho(){
		return limiteX;
	}
	public int getAlto(){
		return limiteY;
	}
	
	public void moverPersonaje(Personaje personaje, Posicion posFinal){
		
		Celda celdaFinal = this.celdas[posFinal.getX()][posFinal.getY()];
		celdaFinal.setPersonaje(personaje);
		Posicion posInicial = personaje.getPosicion();
		this.celdas[posInicial.getX()][posInicial.getY()].setPersonaje(null);
		personaje.setPosicion(posFinal);
	}
}
