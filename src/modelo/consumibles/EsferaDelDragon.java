package modelo.consumibles;

public class EsferaDelDragon implements Consumibles{
	private static final int DURACION_ESFERA_DEL_DRAGON=2;
	private static final double MULTIPLICADOR_PODER_ESFERA_DEL_DRAGON=1.25;
	private static final String NOMBRE="EsferaDelDragon";
	
	public int getDuracionDeEfectoPoder(){
		return DURACION_ESFERA_DEL_DRAGON;
	}
	public int getDuracionDeEfectoVelocidad(){
		return 0;
	}
	public double getIncrementoVida(){
		return 0;
	}
	public double getMultiplicadorPoder(){
		return MULTIPLICADOR_PODER_ESFERA_DEL_DRAGON;
	}
	public double getMultiplicadorVelocidad(){
		return 1;
	}
	public int sumarEsfera(){
		return 1;
	}
	public String getNombre(){
		return NOMBRE;
	}
}
