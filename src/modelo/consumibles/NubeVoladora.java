package modelo.consumibles;

public class NubeVoladora implements Consumibles{
	private static final int DURACION_NUBE_VOLADORA=3;
	private static final double MULTIPLICADOR_VELOCIDAD_NUBE_VOLADORA=2;
	private static final String NOMBRE="Nube";

	public int getDuracionDeEfectoVelocidad(){
		return DURACION_NUBE_VOLADORA;
	}
	public int getDuracionDeEfectoPoder(){
		return 0;
	}
	public double getIncrementoVida(){
		return 0;
	}
	public double getMultiplicadorPoder(){
		return 1;
	}
	public double getMultiplicadorVelocidad(){
		return MULTIPLICADOR_VELOCIDAD_NUBE_VOLADORA;
	}
	public int sumarEsfera(){
		return 0;
	}
	public String getNombre(){
		return NOMBRE;
	}
}
