package modelo.consumibles;

public interface Consumibles {
	double getMultiplicadorVelocidad();
	double getMultiplicadorPoder();
	double getIncrementoVida();
	int getDuracionDeEfectoPoder();
	int getDuracionDeEfectoVelocidad();
	int sumarEsfera();
	String getNombre();
}
