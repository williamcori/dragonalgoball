package modelo.consumibles;

import modelo.tablero.Posicion;
import modelo.tablero.Tablero;

public class AgregaConsumibles {

	private static final int CANTIDAD_CONSUMIBLES=3;
	private static final double PROBABILIDAD_DE_AGREGAR=0.15;
	private Tablero tablero;
	
	public AgregaConsumibles(Tablero tablero){
		this.tablero=tablero;
	}
	public void decidirAlAzarSiAgregarConsumible(){
		int aux=(int) (Math.random()*1000000);
		int coeficiente=(int)(1/PROBABILIDAD_DE_AGREGAR);
		if(aux%coeficiente==0)
			agregarConsumibleAleatorio();
	}
	private void agregarConsumibleAleatorio(){
		int aux=(int) (Math.random()*1000000);
		aux=aux%CANTIDAD_CONSUMIBLES;
		if(aux==0)
			agregarConsumibleEnPosicionAleatoria((Consumibles)new EsferaDelDragon());
		if(aux==1)
			agregarConsumibleEnPosicionAleatoria((Consumibles)new NubeVoladora());
		if(aux==2)
			agregarConsumibleEnPosicionAleatoria((Consumibles)new SemillaDelErmitano());
	}
	private void agregarConsumibleEnPosicionAleatoria(Consumibles consumible){
		Posicion pos=generarPosicionAleatoria(tablero);
		if(tablero.celdaVacia(pos))
			tablero.setConsumibleCelda(pos, consumible);
	}
	private Posicion generarPosicionAleatoria(Tablero tablero) {
		int posX=(int) (Math.random()*1000000);
		int posY=(int) (Math.random()*1000000);
		posX=posX%tablero.getAncho();
		posY=posY%tablero.getAlto();
		return new Posicion(posX,posY);
	}
}
