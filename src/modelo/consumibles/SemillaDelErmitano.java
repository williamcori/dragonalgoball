package modelo.consumibles;

public class SemillaDelErmitano implements Consumibles{
	private static final String NOMBRE="Semillas";
	private static final int INCREMENTO_VIDA_SEMILLA_ERMITANO=100;
	
	public int getDuracionDeEfectoPoder(){
		return 0;
	}
	public int getDuracionDeEfectoVelocidad(){
		return 0;
	}
	public double getIncrementoVida(){
		return INCREMENTO_VIDA_SEMILLA_ERMITANO;
	}
	public double getMultiplicadorPoder(){
		return 1;
	}
	public double getMultiplicadorVelocidad(){
		return 1;
	}
	public int sumarEsfera(){
		return 0;
	}
	public String getNombre(){
		return NOMBRE;
	}
}
