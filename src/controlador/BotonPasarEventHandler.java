package controlador;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import modelo.Juego;
import vista.contenedorJuego.ContenedorSuperiorJuego;
import vista.contenedorJuego.ContenedorTablero;

public class BotonPasarEventHandler implements EventHandler<ActionEvent> {
	Stage stage;
	Juego juego;
	ContenedorSuperiorJuego contSupJuego;
	ContenedorTablero contTablero;
	
	public BotonPasarEventHandler(Stage stage, Juego juego, ContenedorSuperiorJuego contSupJuego, ContenedorTablero contTablero){
		this.stage=stage;
		this.juego=juego;
		this.contSupJuego = contSupJuego;
		this.contTablero = contTablero;
	}
	
	@Override
	public void handle(ActionEvent event){
		juego.getJugadorActual().pasarTurno();
		juego.jugar();
		this.contSupJuego.update();
		this.contTablero.update();
	}
}
