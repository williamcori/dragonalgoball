package controlador;

import javafx.scene.control.Alert;

public class MensajeError {
	
	public MensajeError(String titulo, String mensaje) {
		Alert error = new Alert(Alert.AlertType.WARNING);
		error.setTitle(titulo);
		error.setContentText(mensaje);
		error.setHeaderText(null);
		error.showAndWait();
	}
}
