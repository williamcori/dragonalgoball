package controlador;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.stage.Stage;
import vista.ContenedorAbout;

public class BotonAboutEventHandler implements EventHandler<ActionEvent> {
	
	Stage stage;
	
	public BotonAboutEventHandler(Stage stage) {
		this.stage = stage;
	}

	@Override
	public void handle(ActionEvent actionEvent) {
		ContenedorAbout contenedorAbout = new ContenedorAbout(stage);
		Scene escenaAbout = new Scene(contenedorAbout, stage.getScene().getWidth(), stage.getScene().getHeight());
		stage.setScene(escenaAbout);
		stage.setFullScreen(true);
	}
}
