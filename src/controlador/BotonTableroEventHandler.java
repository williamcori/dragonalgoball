package controlador;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.stage.Stage;
import modelo.tablero.Posicion;
import vista.ContenedorBienvenida;
import vista.contenedorJuego.ContenedorSuperiorJuego;
import vista.contenedorJuego.ContenedorTablero;
import modelo.*;
import modelo.excepciones.*;
import modelo.personaje.Personaje;

public class BotonTableroEventHandler implements EventHandler<ActionEvent> {
	
	Stage stage;
	Juego juego;
	Posicion pos;
	ContenedorTablero cont;
	ContenedorSuperiorJuego contSupJuego;
	
	public BotonTableroEventHandler(Stage stage,Juego juego,Posicion pos,ContenedorTablero cont, ContenedorSuperiorJuego contSupJuego){
		this.stage=stage;
		this.juego=juego;
		this.pos=pos;
		this.cont=cont;
		this.contSupJuego = contSupJuego;
	}
	
	@Override
	public void handle(ActionEvent event){
		Personaje seleccionado=juego.getJugadorActual().getPersonajeSeleccionado();
		String nombre=seleccionado.getNombre();
		String fase=seleccionado.getNombreFaseActual();
		if(juego.getTablero().hayPersonajeEn(pos)){
			Personaje personaje=juego.getTablero().getPersonajeCelda(pos);
			try{
				juego.getJugadorActual().accionarConPersonaje(personaje);
			}catch(KiInsuficiente e) {
				int kiNecesario=seleccionado.getCostoAtaqueEspecial();
				new MensajeError("Ki Insuficiente", nombre+" necesita "+kiNecesario+" puntos de ki para usar su ataque especial.");
			} catch (RangoInvalido e) {
				int distancia=seleccionado.getDistanciaAtaque();
				new MensajeError("Rango Invalido","La distancia de ataque de "+nombre+" "+fase+" es de "+distancia+" casilleros.");
			}catch(PersonajeInmovilizado e){
				new MensajeError("Personaje inmovilizado", nombre+" actualmente esta convertido en chocolate.");
			}catch(AtaqueYaRealizado e){
				new MensajeError("Ataque ya Realizado", "Ya has atacado en este turno. Puedes moverte o pasar turno.");
			}
		}else{
			try{
				juego.getJugadorActual().accionarConPosicion(pos);
			}catch(RangoInvalido e){
				int velocidad=seleccionado.getVelocidad();
				new MensajeError("Rango Invalido","La velocidad de "+nombre+" "+fase+" es de "+velocidad+" casilleros.");				
			}catch(PersonajeInmovilizado e){
				new MensajeError("Personaje inmovilizado", nombre+" actualmente esta convertido en chocolate.");
			}catch(MovimientoYaRealizado e){
				new MensajeError("Movimiento ya Realizado", "Ya te has movido en este turno. Puedes atacar o pasar turno.");
			}
		}
		
		
		cont.update();
		if(juego.juegoTerminado()){
			Jugador ganador = juego.obtenerGanador();
			new MensajeError("Hay ganador", "Gano: "+ganador.getNombreEquipo());
			Scene escena = new Scene(new ContenedorBienvenida(stage));
			boolean pantallaCompleta = stage.isFullScreen();
			stage.setScene(escena);
			stage.setFullScreenExitHint("");
			stage.setFullScreen(pantallaCompleta);
		}
		if(juego.getJugadorActual().terminoSuTurno())
			juego.jugar();
		contSupJuego.update();
	}
	
}
