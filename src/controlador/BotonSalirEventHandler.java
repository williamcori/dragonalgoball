package controlador;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

public class BotonSalirEventHandler implements EventHandler<ActionEvent> {

	@Override
	public void handle(ActionEvent actionEvent) {
		Alert alerta = new Alert(AlertType.CONFIRMATION, "¿Esta seguro que desea salir?", ButtonType.YES, ButtonType.NO);
		alerta.setTitle("Confirmacion de salida");
		alerta.showAndWait();
		
		if(alerta.getResult() == ButtonType.YES) {
			System.exit(0);
		}
	}
}
