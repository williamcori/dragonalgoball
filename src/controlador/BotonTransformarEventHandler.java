package controlador;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import modelo.Juego;
import modelo.excepciones.*;
import vista.contenedorJuego.ContenedorSuperiorJuego;
import vista.contenedorJuego.ContenedorTablero;

public class BotonTransformarEventHandler implements EventHandler<ActionEvent>{
	Stage stage;
	Juego juego;
	ContenedorSuperiorJuego contSupJuego;
	ContenedorTablero contTablero;
	
	public BotonTransformarEventHandler(Stage stage, Juego juego, ContenedorSuperiorJuego contSupJuego, ContenedorTablero contTablero){
		this.stage=stage;
		this.juego=juego;
		this.contSupJuego = contSupJuego;
		this.contTablero = contTablero;
	}
	
	@Override
	public void handle(ActionEvent event){
		String nombrePersonaje=juego.getJugadorActual().getPersonajeSeleccionado().getNombre();
		try{
			juego.getJugadorActual().transformarPersonajeSeleccionado();
		}catch(KiInsuficiente e){
			int kiNecesario=juego.getJugadorActual().getPersonajeSeleccionado().getCostoTransformacion();
			new MensajeError("Ki insuficiente", nombrePersonaje+" necesita "+kiNecesario+" puntos de ki para transformarse.");
		}catch(AbsorcionesInsuficientes e){
			new MensajeError("Absorciones insuficiente", "Cell no puede transformarse antes de realizar 4 absorciones.");
		}catch(PersonajeInmovilizado e){
			new MensajeError("Personaje inmovilizado", nombrePersonaje+" actualmente esta convertido en chocolate.");
		}catch(AmigosFueraDePeligro e){
			new MensajeError("Amigos fuera de peligro", "Gohan no puede transformarse si sus amigos no estan en peligro.");
		}catch(ProtegidoFueraDePeligro e){
			new MensajeError("Protegido fuera de peligro", "Piccolo no puede transformarse si Gohan no esta en peligro.");			
		}catch(PersonajeEnUltimaFase e){
			new MensajeError("Personaje en ultima fase", nombrePersonaje+" esta en su utima fase.");
		}
		this.contSupJuego.update();
		this.contTablero.update();
	}
}
