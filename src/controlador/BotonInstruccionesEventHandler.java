package controlador;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.stage.Stage;
import vista.ContenedorInstrucciones;

public class BotonInstruccionesEventHandler implements EventHandler<ActionEvent> {

	Stage stage;
	
	public BotonInstruccionesEventHandler(Stage stage) {
		this.stage = stage;
	}
	
	@Override
	public void handle(ActionEvent event) {
		ContenedorInstrucciones contenedorInstrucciones = new ContenedorInstrucciones(stage);
		Scene escenaInstrucciones = new Scene(contenedorInstrucciones, stage.getScene().getWidth(), stage.getScene().getHeight());
		stage.setScene(escenaInstrucciones);
		stage.setFullScreen(true);
	}
}
