package controlador;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import modelo.Juego;

public class BotonAtaqueEspecialEventHandler implements EventHandler<ActionEvent>{
	Stage stage;
	Juego juego;
	
	public BotonAtaqueEspecialEventHandler(Stage stage, Juego juego){
		this.stage=stage;
		this.juego=juego;
	}
	
	@Override
	public void handle(ActionEvent event){
		juego.getJugadorActual().activarAtaqueEspecial();
	}
}
