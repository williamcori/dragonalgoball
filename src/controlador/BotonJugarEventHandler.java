package controlador;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import modelo.Juego;
import vista.contenedorJuego.ContenedorJuego;

public class BotonJugarEventHandler implements EventHandler<ActionEvent> {

	Stage stage;
	Juego juego;
	private MediaPlayer reproductor;
	
	public BotonJugarEventHandler(Stage stage, Juego juego,MediaPlayer reproductor) {
		this.reproductor = reproductor;
		this.stage = stage;
		this.juego = juego;
	}

	@Override
	public void handle(ActionEvent event) { 
		reproductor.stop();
		juego.jugar();
		ContenedorJuego contenedorJuego = new ContenedorJuego(stage, juego);
		Scene escenaJuego = new Scene(contenedorJuego, stage.getScene().getWidth(), stage.getScene().getHeight());
		stage.setScene(escenaJuego);
		stage.setFullScreen(true);
	}
}
