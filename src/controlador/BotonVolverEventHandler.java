package controlador;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.stage.Stage;
import vista.ContenedorBienvenida;

public class BotonVolverEventHandler implements EventHandler<ActionEvent> {
	
	Stage stage;
	
	public BotonVolverEventHandler(Stage stage) {
		this.stage = stage;
	}

	@Override
	public void handle(ActionEvent event) {
		ContenedorBienvenida contenedorBienvenida = new ContenedorBienvenida(stage);
		Scene escenaBienvenida = new Scene(contenedorBienvenida, stage.getScene().getWidth(), stage.getScene().getHeight());	
		stage.setScene(escenaBienvenida);
		stage.setFullScreen(true);
	}
}
