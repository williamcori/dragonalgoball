package vista;


import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.input.KeyCombination;
import javafx.stage.Stage;

public class Main extends Application {

	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage stage) throws Exception {
		stage.setTitle("DragonAlgoBall");
		
		ContenedorBienvenida contenedorBienvenida = new ContenedorBienvenida(stage);
		Scene escenaBienvenida = new Scene(contenedorBienvenida, 640, 480);
		
		stage.setScene(escenaBienvenida);
		stage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
		stage.setFullScreen(true);
		stage.show();
	}
}
