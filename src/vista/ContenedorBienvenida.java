package vista;

import java.io.File;

import controlador.BotonAboutEventHandler;
import controlador.BotonInstruccionesEventHandler;
import controlador.BotonJugarEventHandler;
import controlador.BotonSalirEventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import modelo.Juego;

public class ContenedorBienvenida extends VBox{

	private static double ANCHO = 220.0, ALTO = 60.0;
	
	Stage stage;
	
	public ContenedorBienvenida(Stage stage) {
		super();
		this.stage = stage;
		this.setAlignment(Pos.CENTER);
		this.setSpacing(20);
		this.setPadding(new Insets(25));
		
		String musicFile = "sonidos/intro.mp3";
		Media sound = new Media(new File(musicFile).toURI().toString());
		MediaPlayer mediaPlayer = new MediaPlayer(sound);
		mediaPlayer.play();
		
		Image fondo = new Image("file:imagenes/fondos/fondo_inicio.jpg");
        BackgroundImage imagenDeFondo = new BackgroundImage(fondo, BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        this.setBackground(new Background(imagenDeFondo));
        
        Image imagenTitulo = new Image("file:imagenes/fondos/title.png");
        ImageView view = new ImageView();
        view.setImage(imagenTitulo);
        
		Label bienvenida = new Label();
		bienvenida.setFont(Font.font("Forte", FontWeight.BOLD, 40));
		bienvenida.setText("Elija una opcion");
		bienvenida.setTextFill(Color.web("#FFFFFF"));
		
		Button botonJugar = new Button();
		botonJugar.setText("Jugar");
		botonJugar.setFont(Font.font("Forte", FontWeight.BOLD, 20));
		BotonJugarEventHandler botonJugarEventHandler = new BotonJugarEventHandler(stage,new Juego(),mediaPlayer);
		botonJugar.setOnAction(botonJugarEventHandler);
		botonJugar.setPrefSize(ANCHO, ALTO);
		
		Button botonInstrucciones = new Button();
		botonInstrucciones.setText("Instrucciones");
		botonInstrucciones.setFont(Font.font("Forte", FontWeight.BOLD, 20));
		BotonInstruccionesEventHandler botonInstruccionesEventHandler = new BotonInstruccionesEventHandler(stage);
		botonInstrucciones.setOnAction(botonInstruccionesEventHandler);
		botonInstrucciones.setPrefSize(ANCHO, ALTO);
		
		Button botonAbout = new Button();
		botonAbout.setText("Acerca de");
		botonAbout.setFont(Font.font("Forte", FontWeight.BOLD, 20));
		BotonAboutEventHandler botonAboutEventHandler = new BotonAboutEventHandler(stage);
		botonAbout.setOnAction(botonAboutEventHandler);
		botonAbout.setPrefSize(ANCHO, ALTO);
		
		Button botonSalir = new Button();
		botonSalir.setText("Salir");
		botonSalir.setFont(Font.font("Forte", FontWeight.BOLD, 20));
		BotonSalirEventHandler botonSalirEventHandler = new BotonSalirEventHandler();
		botonSalir.setOnAction(botonSalirEventHandler);
		botonSalir.setPrefSize(ANCHO, ALTO);
		
		this.getChildren().addAll(view,bienvenida, botonJugar, botonInstrucciones, botonAbout, botonSalir);
	}
}
