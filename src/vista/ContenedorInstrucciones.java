package vista;

import controlador.BotonVolverEventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class ContenedorInstrucciones extends VBox {
	private static double ANCHO = 220.0,ALTO = 60.0;
	Stage stage;
	
	public ContenedorInstrucciones(Stage stage) {
		super();
		this.stage = stage;
		this.setAlignment(Pos.CENTER);
		this.setSpacing(20);
		this.setPadding(new Insets(25));
		
		Image fondo = new Image("file:imagenes/fondos/torneo.jpg");
        BackgroundImage imagenDeFondo = new BackgroundImage(fondo, BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        this.setBackground(new Background(imagenDeFondo));
		
		Label info = new Label();
		info.setText("Instrucciones\n"
				+ "Jugabilidad\n" + 
				"Es un juego por turnos. Hay 2 jugadores, cada jugador comienza la partida con sus 3 personajes.\n" + 
				"Sistema de Ataque:\n" + 
				"- Cada personaje tiene un ataque básico (no cuesta Ki) y un ataque especial\n" + 
				"(tiene costo) que puede aplicar un efecto.\n" + 
				"- Ataque básico: el daño del ataque básico es igual al poder de pelea actual del\n" + 
				"personaje (el poder de pelea no cambia más que en transformaciones, pero el\n" + 
				"ataque puede variar por algún efecto ).\n" + 
				"- [IMPORTANTE] Al atacar a un enemigo de mayor poder de pelea, el daño\n" + 
				"de dicho ataque se reduce un 20%.\n" + 
				"Ki:\n" + 
				"- El Ki se aumenta solo en cada personaje, en una cantidad de 5 por turno\n" + 
				"(cuando empieza el turno se aumenta).\n" + 
				"Acciones en turno:\n" + 
				"- En un mismo turno, se puede realizar una acción de ataque y una acción de\n" + 
				"movilidad, no necesariamente del mismo personaje.\n" + 
				"-\n" + 
				"El juego elige al azar qué jugador comienza. Cada jugador inicia en el extremo opuesto al\n" + 
				"otro con sus 3 personajes juntos.\n" + 
				"Tablero\n" + 
				"El juego tiene lugar en un tablero compuesto de casilleros. El tamaño, forma y cantidad de\n" + 
				"casilleros del tablero queda a definir por cada grupo y acordado con su ayudante.\n" + 
				"Todos los personajes ocupan 1 casillero en cualquiera de sus modos. No puede haber\n" + 
				"más de 1 personaje en un casillero. Hay 1 o 0, nunca 2, 3, etc. independientemente del\n" + 
				"equipo al que pertenezcan.\n" + 
				"Los personaje se desplazan por el tablero de casillero en casillero. Cada punto de su\n" + 
				"velocidad de desplazamiento representa 1 casillero.\n" + 
				"Fin del juego\n" + 
				"Para ganar se debe destruir a todos los personajes enemigos.\n" + 
				"[OPCIONAL] Fin del juego alternativo (suma puntaje extra)\n" + 
				"Para ganar el juego sin tener que matar a todos los enemigos, se pueden capturar 7\n" + 
				"esferas del dragón entre cualquiera de los 3 personajes que tenga. (es decir el conteo de\n" + 
				"esferas es global, no de cada personaje particular.).");
		info.setFont(Font.font("Forte", FontWeight.BOLD, 18));
		info.setTextFill(Color.web("#000000"));
		info.setStyle("-fx-background-color: white;");
		Button botonVolver = new Button();
		botonVolver.setText("Volver");
		botonVolver.setFont(Font.font("Forte", FontWeight.BOLD, 20));
		botonVolver.setMinSize(ANCHO, ALTO);
		botonVolver.setMaxSize(ANCHO, ALTO);
		BotonVolverEventHandler botonVolverEventHandler = new BotonVolverEventHandler(stage);
		botonVolver.setOnAction(botonVolverEventHandler);
		
		this.getChildren().addAll(info, botonVolver);
	}
}
