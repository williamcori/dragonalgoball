package vista.contenedorJuego;

import modelo.*;

import java.io.File;

import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

public class ContenedorJuego extends BorderPane {
	
	Stage stage;
	
	public ContenedorJuego(Stage stage, Juego juego) {
		
		super();
		this.stage = stage;
		
		Image fondo = new Image("file:imagenes/fondos/fondo_batalla.png");
        BackgroundImage imagenDeFondo = new BackgroundImage(fondo, BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        this.setBackground(new Background(imagenDeFondo));
        
        String musicFile = "sonidos/battle.mp3";
		Media sound = new Media(new File(musicFile).toURI().toString());
		MediaPlayer mediaPlayer = new MediaPlayer(sound);
		mediaPlayer.setCycleCount(MediaPlayer.INDEFINITE);
		mediaPlayer.play();
		
		ContenedorSuperiorJuego contSupJuego = new ContenedorSuperiorJuego(stage, juego);
		this.setTop(contSupJuego);
		ContenedorTablero contTablero = new ContenedorTablero(stage, juego, contSupJuego);
		this.setCenter(contTablero);
		this.setLeft(new ContenedorControles(stage, juego, contSupJuego, contTablero));
	}
}
