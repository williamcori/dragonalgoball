package vista.contenedorJuego;

import modelo.*;
import modelo.personaje.Personaje;

import java.util.ArrayList;
import java.util.List;


import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class ContenedorSuperiorJuego extends BorderPane{
	
	Stage stage;
	Juego juego;
	List<ContenedorDatosPersonaje> listaDatosPersonajes;
	Label labelTurno;
	Label labelPersonajeSeleccionado;

	public ContenedorSuperiorJuego(Stage stage, Juego juego) {
		
		super();
		this.listaDatosPersonajes = new ArrayList<ContenedorDatosPersonaje>();
		this.stage = stage;
		this.juego = juego;
		//this.setSpacing(50);
		//this.setAlignment(Pos.CENTER);
		
		
		Jugador[] jugadores= juego.getJugadores();
		Personaje[] pjs1=jugadores[0].getPersonajes();
		Personaje[] pjs2=jugadores[1].getPersonajes();
		
		VBox pjsIzquierda = new VBox();
		pjsIzquierda.setAlignment(Pos.CENTER_LEFT);
		
		for(Personaje personaje:pjs1) {
			ContenedorDatosPersonaje pj = new ContenedorDatosPersonaje(stage,personaje);
			pj.posicionarIzquierda();
			this.listaDatosPersonajes.add(pj);
			pjsIzquierda.getChildren().add(pj);
		}
		
		VBox pjsDerecha = new VBox();
		pjsDerecha.setAlignment(Pos.CENTER_RIGHT);
		
		for(Personaje personaje:pjs2) {
			ContenedorDatosPersonaje pj = new ContenedorDatosPersonaje(stage,personaje);
			pj.posicionarDerecha();
			this.listaDatosPersonajes.add(pj);
			pjsDerecha.getChildren().add(pj);
		}
		
		VBox turno = new VBox();
		labelTurno = new Label("Turno de: "+juego.getJugadorActual().getNombreEquipo());
		labelTurno.setFont(Font.font("Forte", FontWeight.BOLD, 40));
		labelPersonajeSeleccionado = new Label(juego.getJugadorActual().getPersonajeSeleccionado().getNombre()+" seleccionado");
		labelPersonajeSeleccionado.setFont(Font.font("Forte", FontWeight.BOLD,25));
		turno.setAlignment(Pos.CENTER);
		turno.getChildren().addAll(labelTurno,labelPersonajeSeleccionado);
		
		this.setLeft(pjsIzquierda);
		this.setRight(pjsDerecha);
		this.setCenter(turno);
		
	}
	
	public void update() {
		this.labelTurno.setText("Turno de: "+juego.getJugadorActual().getNombreEquipo());
		this.labelPersonajeSeleccionado.setText(juego.getJugadorActual().getPersonajeSeleccionado().getNombre()+" seleccionado");
		for(ContenedorDatosPersonaje contenedor : this.listaDatosPersonajes) {
			contenedor.update();
		}
				
	}
}
