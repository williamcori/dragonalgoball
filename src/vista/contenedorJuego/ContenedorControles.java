package vista.contenedorJuego;

import modelo.*;
import javafx.geometry.Pos;
import controlador.BotonAtaqueBasicoEventHandler;
import controlador.BotonAtaqueEspecialEventHandler;
import controlador.BotonPasarEventHandler;
import controlador.BotonTransformarEventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class ContenedorControles extends VBox{
	
	Stage stage;
	Juego juego;
	private static double ANCHO = 220.0,ALTO = 60.0;
	public ContenedorControles(Stage stage, Juego juego, ContenedorSuperiorJuego contSupJuego, ContenedorTablero contTablero) {
		
		super();
		this.stage = stage;
		this.setPadding(new Insets(10));
		this.setSpacing(15);
		this.setAlignment(Pos.CENTER);
		
		Button botonAtaqueBasico= new Button("Activar ataque basico");
		botonAtaqueBasico.setFont(Font.font("Forte", FontWeight.BOLD, 20));
		botonAtaqueBasico.setMinSize(ANCHO, ALTO);
		botonAtaqueBasico.setMaxSize(ANCHO, ALTO);
		getChildren().add(botonAtaqueBasico);
		BotonAtaqueBasicoEventHandler botonAtaqueBasicoEventHandler = new BotonAtaqueBasicoEventHandler(stage,juego);
		botonAtaqueBasico.setOnAction(botonAtaqueBasicoEventHandler);
		
		Button botonAtaqueEspecial= new Button("Activar ataque especial");
		botonAtaqueEspecial.setFont(Font.font("Forte", FontWeight.BOLD, 19));
		botonAtaqueEspecial.setMinSize(ANCHO, ALTO);
		botonAtaqueEspecial.setMaxSize(ANCHO, ALTO);
		getChildren().add(botonAtaqueEspecial);
		BotonAtaqueEspecialEventHandler botonAtaqueEspecialEventHandler = new BotonAtaqueEspecialEventHandler(stage,juego);
		botonAtaqueEspecial.setOnAction(botonAtaqueEspecialEventHandler);
		
		Button botonTransformar= new Button("Transformar");
		botonTransformar.setFont(Font.font("Forte", FontWeight.BOLD, 20));
		botonTransformar.setMinSize(ANCHO, ALTO);
		botonTransformar.setMaxSize(ANCHO, ALTO);
		getChildren().add(botonTransformar);
		BotonTransformarEventHandler botonTransformarEventHandler = new BotonTransformarEventHandler(stage,juego, contSupJuego, contTablero);
		botonTransformar.setOnAction(botonTransformarEventHandler);
		
		Button botonPasar= new Button("Pasar Turno");
		botonPasar.setFont(Font.font("Forte", FontWeight.BOLD, 20));
		botonPasar.setMinSize(ANCHO, ALTO);
		botonPasar.setMaxSize(ANCHO, ALTO);
		getChildren().add(botonPasar);
		BotonPasarEventHandler botonPasarEventHandler = new BotonPasarEventHandler(stage,juego, contSupJuego,contTablero);
		botonPasar.setOnAction(botonPasarEventHandler);
				

	}
}
