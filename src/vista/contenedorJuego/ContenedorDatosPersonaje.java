package vista.contenedorJuego;

import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.geometry.Pos;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.stage.Stage;
import modelo.personaje.Personaje;

public class ContenedorDatosPersonaje extends GridPane{
	
	private static double ALTURA_BARRA = 30.0, ANCHO_BARRA = 400.0;
	Stage stage;
	private Personaje personaje;
	private Label logo;
	private Label labelVida;
	private ProgressBar barraVida;
	private Label labelKi;
	private ProgressBar barraKi;
	
	public ContenedorDatosPersonaje(Stage stage, Personaje personaje){
		super();
		this.stage = stage;
		this.personaje=personaje;
		this.setAlignment(Pos.CENTER_LEFT);
		this.setHgap(5);
		this.setVgap(5);
		this.setPadding(new Insets(5));
		this.getStylesheets().add("file:css/contenedorJuego.css");
		
		
		this.logo = new Label(personaje.getNombre());
		logo.setFont(Font.font("Forte", FontWeight.BOLD, 20));
		logo.setTextFill(Color.web("#000000"));
		//Falta setear imagen para los personajes
		this.logo.setMinSize(ALTURA_BARRA * 3, ALTURA_BARRA * 2);
		this.logo.setMaxSize(ALTURA_BARRA * 3, ALTURA_BARRA * 2);
		
		String vida=String.valueOf(personaje.getVida());
		this.labelVida = new Label(vida);
		this.labelVida.setFont(Font.font("Forte", FontWeight.BOLD, 18));
		this.labelVida.setAlignment(Pos.CENTER);
		this.labelVida.setPrefSize(ANCHO_BARRA, ALTURA_BARRA);
		
		this.barraVida = new ProgressBar(1.0);
		this.barraVida.setPrefSize(ANCHO_BARRA, ALTURA_BARRA);
		this.barraVida.getStyleClass().add("green-bar");
		
		String ki=String.valueOf(personaje.getKi());
		this.labelKi = new Label(ki);
		this.labelKi.setFont(Font.font("Forte", FontWeight.BOLD, 18));
		this.labelKi.setAlignment(Pos.CENTER);
		this.setPrefSize(ANCHO_BARRA, ALTURA_BARRA);
		
		this.barraKi = new ProgressBar(0.0);
		this.barraKi.setPrefSize(ANCHO_BARRA, ALTURA_BARRA);
	}
	
	public void posicionarIzquierda(){
		this.getChildren().clear();
		
		this.add(this.logo, 0, 0, 1, 2);
		
		this.add(new StackPane(this.barraVida, this.labelVida), 1, 0);
		this.add(new StackPane(this.barraKi, this.labelKi), 1, 1);
	}
	
	public void posicionarDerecha(){
		this.getChildren().clear();
		
		this.add(this.logo, 1, 0, 1, 2);
		
		this.barraVida.setRotate(180.0);
		this.barraKi.setRotate(180.0);
		this.add(new StackPane(this.barraVida, this.labelVida), 0, 0);
		this.add(new StackPane(this.barraKi, this.labelKi), 0, 1);
	}	
	
	public void update(){
		String vida=String.valueOf(personaje.getVida());
		String ki=String.valueOf(personaje.getKi());
		this.labelVida.setText(vida);
		this.labelKi.setText(ki);
		this.barraVida.setProgress(personaje.getVida() / personaje.getVidaInicial());
		this.barraKi.setProgress( ((double)personaje.getKi()) / personaje.getKiMaximo());
	}
}
