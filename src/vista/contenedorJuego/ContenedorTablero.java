package vista.contenedorJuego;

import modelo.*;
import modelo.consumibles.Consumibles;
import modelo.personaje.Personaje;
import modelo.tablero.Posicion;
import modelo.tablero.Tablero;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.GridPane;
import controlador.BotonTableroEventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class ContenedorTablero extends GridPane{
	
	private static double TAM_CELDA = 120.0;
	private Juego juego;
	private Button[][] botones;
	
	public ContenedorTablero(Stage stage, Juego juego, ContenedorSuperiorJuego contSupJuego) {
		super();
		this.juego=juego;
		this.setAlignment(Pos.CENTER);
		int ancho=juego.getTablero().getAncho();
		int alto=juego.getTablero().getAlto();
		botones = new Button[ancho][alto];
		
		for(int j = 0; j < alto; j++){
			for(int i = 0; i < ancho; i++){
				Button boton = new Button(" ");
				boton.setMinSize(TAM_CELDA, TAM_CELDA);
				boton.setMaxSize(TAM_CELDA, TAM_CELDA);
				Image fondo = new Image("file:imagenes/fondos/grassTile.jpg");
		        BackgroundImage imagenDeFondo = new BackgroundImage(fondo, BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
		        boton.setBackground(new Background(imagenDeFondo));
				botones[i][j] = boton;
				this.add(boton, i, j);
				Posicion pos= new Posicion(i,j);
				BotonTableroEventHandler botonTableroEventHandler = new BotonTableroEventHandler(stage,juego,pos, this, contSupJuego);
				boton.setOnAction(botonTableroEventHandler);
			}
		}
		update();
	}
	public void update(){                
		
		
		
		Tablero tablero=juego.getTablero();
		int ancho=tablero.getAncho();
		int alto=tablero.getAlto();
		for(int i=0;i<ancho;i++){
			for(int j=0;j<alto;j++){
				Posicion pos=new Posicion(i,j);
				String texto=" ";
				Personaje personaje=tablero.getPersonajeCelda(pos);
				Consumibles consumible=tablero.getConsumibleCelda(pos);
				if(personaje!=null){
					texto=personaje.getNombre()+personaje.getEstado().getFase().getNombre();
					if(personaje.inmovilizado())
						texto="Chocolate";
				}
				else if(consumible!=null)
					texto=consumible.getNombre();
				(botones[pos.getX()][pos.getY()]).setGraphic(obtenerImagen(texto));
			}
		}
	}
	
	private ImageView obtenerImagen(String nombre){
		Image imagenTitulo = new Image("file:imagenes/personajes/"+nombre+".png");
        ImageView view = new ImageView();
        view.setImage(imagenTitulo);
        return view;
	}
}
