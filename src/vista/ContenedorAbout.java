package vista;

import controlador.BotonVolverEventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class ContenedorAbout extends VBox {
	private static double ANCHO = 220.0,ALTO = 60.0;
	Stage stage;
	
	public ContenedorAbout(Stage stage) {
		super();
		this.stage = stage;
		this.setAlignment(Pos.CENTER);
		this.setSpacing(20);
		this.setPadding(new Insets(25));
		
		Image fondo = new Image("file:imagenes/fondos/namek.png");
        BackgroundImage imagenDeFondo = new BackgroundImage(fondo, BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        this.setBackground(new Background(imagenDeFondo));
		
		Label info = new Label();
		info.setText("Desarrollado por alumnos de Algoritmos y Programacion 3 de la FIUBA");
		info.setFont(Font.font("Forte", FontWeight.BOLD, 30));
		info.setTextFill(Color.web("#000000"));
		Label info2 = new Label();
		info2.setText("Integrantes:\nPatricio Avigliano\nWilliam Cori\nKevin Cajachuan\nGuillermo Condori");
		info2.setFont(Font.font("Forte", FontWeight.BOLD, 30));
		info2.setTextFill(Color.web("#000000"));
		
		Button botonVolver = new Button();
		botonVolver.setText("Volver");
		botonVolver.setFont(Font.font("Forte", FontWeight.BOLD, 20));
		botonVolver.setMinSize(ANCHO, ALTO);
		botonVolver.setMaxSize(ANCHO, ALTO);
		BotonVolverEventHandler botonVolverEventHandler = new BotonVolverEventHandler(stage);
		botonVolver.setOnAction(botonVolverEventHandler);
		
		this.getChildren().addAll(info,info2, botonVolver);
	}
}
