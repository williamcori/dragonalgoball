package modelo.pruebasIntegracion;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import modelo.excepciones.*;
import modelo.personaje.*;
import modelo.tablero.Posicion;
import modelo.tablero.Tablero;

public class SegundaEntregaTest {
	private static final int ANCHO=10;	
	private static final int ALTO=10;	

	// Item 1
	@Test(expected = AmigosFueraDePeligro.class)
	public void gohanNoLlegaASuUltimaFaseSiGokuYPiccoloTienenMasDe30PorcientoDeVida(){
		Tablero tablero=new Tablero(ANCHO, ALTO);
		Personaje gohan= new Gohan();
		Personaje goku= new Goku();
		Personaje piccolo= new Piccolo();
		Posicion posGohan=new Posicion(5,2);
		Posicion posGoku=new Posicion(1,2);
		Posicion posPiccolo=new Posicion(2,2);
		gohan.setTablero(tablero);
		goku.setTablero(tablero);
		piccolo.setTablero(tablero);
		gohan.ubicarPersonaje(posGohan);
		goku.ubicarPersonaje(posGoku);
		piccolo.ubicarPersonaje(posPiccolo);
		Personaje[] amigos = {goku,piccolo};
		((Preocupado) gohan).setAmigos(amigos);
		for(int i = 0; i < 15; i++)
			gohan.aumentarKi();
		gohan.cambiarFase();
		gohan.cambiarFase();
	}
	// Item 2
	@Test
	public void gohanLlegaASuUltimaFaseSiGokuYPiccoloTienenMenosDe30PorcientoDeVida(){
		Tablero tablero=new Tablero(ANCHO, ALTO);
		Personaje gohan= new Gohan();
		Personaje goku= new Goku();
		Personaje piccolo= new Piccolo();
		Posicion posGohan=new Posicion(5,2);
		Posicion posGoku=new Posicion(1,2);
		Posicion posPiccolo=new Posicion(2,2);
		gohan.setTablero(tablero);
		goku.setTablero(tablero);
		piccolo.setTablero(tablero);
		gohan.ubicarPersonaje(posGohan);
		goku.ubicarPersonaje(posGoku);
		piccolo.ubicarPersonaje(posPiccolo);
		Personaje[] amigos = {goku,piccolo};
		((Preocupado) gohan).setAmigos(amigos);
		for(int i = 0; i < 15; i++)
			gohan.aumentarKi();
		goku.disminuirVida(400);
		piccolo.disminuirVida(400);
		gohan.cambiarFase();
		gohan.cambiarFase();
		assertEquals("SS2", gohan.getNombreFaseActual());
	}
	// Item 3
	@Test(expected = ProtegidoFueraDePeligro.class)
	public void piccoloNoLlegaASuUltimaFaseSiGohanTienenMasDe20PorcientoDeVida(){
		Tablero tablero=new Tablero(ANCHO, ALTO);
		Personaje gohan= new Gohan();
		Personaje piccolo= new Piccolo();
		Posicion posGohan=new Posicion(5,2);
		Posicion posPiccolo=new Posicion(2,2);
		gohan.setTablero(tablero);
		piccolo.setTablero(tablero);
		gohan.ubicarPersonaje(posGohan);
		piccolo.ubicarPersonaje(posPiccolo);
		for(int i = 0; i < 15; i++)
			piccolo.aumentarKi();
		((Protector) piccolo).setProtegido(gohan);
		piccolo.cambiarFase();
		piccolo.cambiarFase();
	}
	// Item 4
	@Test
	public void piccoloLlegaASuUltimaFaseSiGohanTienenMenosDe20PorcientoDeVida(){
		Tablero tablero=new Tablero(ANCHO, ALTO);
		Personaje gohan= new Gohan();
		Personaje piccolo= new Piccolo();
		Posicion posGohan=new Posicion(5,2);
		Posicion posPiccolo=new Posicion(2,2);
		gohan.setTablero(tablero);
		piccolo.setTablero(tablero);
		gohan.ubicarPersonaje(posGohan);
		piccolo.ubicarPersonaje(posPiccolo);
		for(int i = 0; i < 15; i++)
			piccolo.aumentarKi();
		((Protector) piccolo).setProtegido(gohan);
		gohan.disminuirVida(290);
		piccolo.cambiarFase();
		piccolo.cambiarFase();
		assertEquals("Protector", piccolo.getNombreFaseActual());
	}
	// Item 5
	@Test(expected = AbsorcionesInsuficientes.class)
	public void cellNoPuedeCambiarDeFaseSinAntesAbsorberVida(){
		Tablero tablero=new Tablero(ANCHO, ALTO);
		Personaje cell= new Cell();
		Posicion pos=new Posicion(2,2);
		cell.setTablero(tablero);
		cell.ubicarPersonaje(pos);
		cell.cambiarFase();
	}
	// Item 6
	@Test
	public void cellAumentaSuVidaLoMismoQueSuEnemigoLaDisminuyeAlAbsorberSuVida(){
		Tablero tablero=new Tablero(ANCHO, ALTO);
		Personaje cell= new Cell();
		Personaje piccolo= new Piccolo();
		Posicion posCell=new Posicion(5,2);
		Posicion posPiccolo=new Posicion(4,2);
		cell.setTablero(tablero);
		piccolo.setTablero(tablero);
		cell.ubicarPersonaje(posCell);
		piccolo.ubicarPersonaje(posPiccolo);
		for(int i = 0; i < 15; i++)
			cell.aumentarKi();
		cell.disminuirVida(200);
		cell.ataqueEspecial(piccolo);
		assertEquals(480, (int)piccolo.getVida());
		assertEquals(320, (int)cell.getVida());
	}
	// Item 7
	@Test(expected = AbsorcionesInsuficientes.class)
	public void cellNoCambiaDeFaseSiNoAbsorbe4Veces(){
		Tablero tablero=new Tablero(ANCHO, ALTO);
		Personaje cell= new Cell();
		Personaje piccolo= new Piccolo();
		Posicion posCell=new Posicion(5,2);
		Posicion posPiccolo=new Posicion(4,2);
		cell.setTablero(tablero);
		piccolo.setTablero(tablero);
		cell.ubicarPersonaje(posCell);
		piccolo.ubicarPersonaje(posPiccolo);
		for(int i = 0; i < 15; i++)
			cell.aumentarKi();
		for(int i=0;i!=3;i++)
			cell.ataqueEspecial(piccolo);
		cell.cambiarFase();
	}
	@Test
	public void cellCambiaDeFaseSiAbsore4veces(){
		Tablero tablero=new Tablero(ANCHO, ALTO);
		Personaje cell= new Cell();
		Personaje piccolo= new Piccolo();
		Posicion posCell=new Posicion(5,2);
		Posicion posPiccolo=new Posicion(4,2);
		cell.setTablero(tablero);
		piccolo.setTablero(tablero);
		cell.ubicarPersonaje(posCell);
		piccolo.ubicarPersonaje(posPiccolo);
		for(int i = 0; i < 15; i++)
			cell.aumentarKi();
		for(int i=0;i!=4;i++){
			cell.ataqueEspecial(piccolo);
		}
		cell.cambiarFase();
		assertEquals("SemiPerfecto", cell.getNombreFaseActual());
	}
	@Test(expected=AbsorcionesInsuficientes.class)
	public void cellNoCambiaAUltimaFaseSiNoAbsore8veces(){
		Tablero tablero=new Tablero(ANCHO, ALTO);
		Personaje cell= new Cell();
		Personaje piccolo= new Piccolo();
		Posicion posCell=new Posicion(5,2);
		Posicion posPiccolo=new Posicion(4,2);
		cell.setTablero(tablero);
		piccolo.setTablero(tablero);
		cell.ubicarPersonaje(posCell);
		piccolo.ubicarPersonaje(posPiccolo);
		for(int i = 0; i < 15; i++)
			cell.aumentarKi();
		cell.disminuirVida(200);
		for(int i=0;i!=7;i++)
			cell.ataqueEspecial(piccolo);
		cell.cambiarFase();
		cell.cambiarFase();
	}
	@Test
	public void cellCambiaAUltimaFaseSiAbsore8veces(){
		Tablero tablero=new Tablero(ANCHO, ALTO);
		Personaje cell= new Cell();
		Personaje piccolo= new Piccolo();
		Posicion posCell=new Posicion(5,2);
		Posicion posPiccolo=new Posicion(4,2);
		cell.setTablero(tablero);
		piccolo.setTablero(tablero);
		cell.ubicarPersonaje(posCell);
		piccolo.ubicarPersonaje(posPiccolo);
		for(int i = 0; i < 15; i++)
			cell.aumentarKi();
		cell.disminuirVida(200);
		for(int i=0;i!=8;i++)
			cell.ataqueEspecial(piccolo);
		cell.cambiarFase();
		cell.cambiarFase();
		assertEquals("Perfecto", cell.getNombreFaseActual());
	}
	// Item 8
	@Test(expected = PersonajeInmovilizado.class)
	public void personajeConvertidoEnChocolateNoPuedeMoverse(){
		Tablero tablero=new Tablero(ANCHO, ALTO);
		Personaje majinboo= new MajinBoo();
		Personaje piccolo= new Piccolo();
		Posicion posMajinboo=new Posicion(5,2);
		Posicion posPiccolo=new Posicion(4,2);
		Posicion posPrueba= new Posicion(4,3);
		majinboo.setTablero(tablero);
		piccolo.setTablero(tablero);
		majinboo.ubicarPersonaje(posMajinboo);
		piccolo.ubicarPersonaje(posPiccolo);
		for(int i = 0; i < 15; i++)
			majinboo.aumentarKi();
		for(int i = 0; i < 15; i++)
			piccolo.aumentarKi();		
		majinboo.ataqueEspecial(piccolo);
		piccolo.mover(posPrueba);
	}
	@Test(expected = PersonajeInmovilizado.class)
	public void personajeConvertidoEnChocolateNoPuedeRealizarAtaqueBasico(){
		Tablero tablero=new Tablero(ANCHO, ALTO);
		Personaje majinboo= new MajinBoo();
		Personaje piccolo= new Piccolo();
		Posicion posMajinboo=new Posicion(5,2);
		Posicion posPiccolo=new Posicion(4,2);
		majinboo.setTablero(tablero);
		piccolo.setTablero(tablero);
		majinboo.ubicarPersonaje(posMajinboo);
		piccolo.ubicarPersonaje(posPiccolo);
		for(int i = 0; i < 15; i++)
			majinboo.aumentarKi();
		for(int i = 0; i < 15; i++)
			piccolo.aumentarKi();		
		majinboo.ataqueEspecial(piccolo);
		piccolo.ataqueBasico(majinboo);
	}
	@Test(expected = PersonajeInmovilizado.class)
	public void personajeConvertidoEnChocolateNoPuedeRealizarAtaqueEspecialNiAumentarKi(){
		Tablero tablero=new Tablero(ANCHO, ALTO);
		Personaje majinboo= new MajinBoo();
		Personaje piccolo= new Piccolo();
		Posicion posMajinboo=new Posicion(5,2);
		Posicion posPiccolo=new Posicion(4,2);
		majinboo.setTablero(tablero);
		piccolo.setTablero(tablero);
		majinboo.ubicarPersonaje(posMajinboo);
		piccolo.ubicarPersonaje(posPiccolo);
		for(int i = 0; i < 15; i++)
			majinboo.aumentarKi();
		for(int i = 0; i < 15; i++)
			piccolo.aumentarKi();		
		int kiAntesDeInmovilizar=piccolo.getKi();
		majinboo.ataqueEspecial(piccolo);
		for(int i = 0; i < 15; i++)
			piccolo.aumentarKi();
		assertEquals(kiAntesDeInmovilizar, piccolo.getKi());		
		piccolo.ataqueEspecial(majinboo);
	}
	@Test(expected = PersonajeInmovilizado.class)
	public void personajeConvertidoEnChocolateNoPuedeTransformarse(){
		Tablero tablero=new Tablero(ANCHO, ALTO);
		Personaje majinboo= new MajinBoo();
		Personaje piccolo= new Piccolo();
		Posicion posMajinboo=new Posicion(5,2);
		Posicion posPiccolo=new Posicion(4,2);
		majinboo.setTablero(tablero);
		piccolo.setTablero(tablero);
		majinboo.ubicarPersonaje(posMajinboo);
		piccolo.ubicarPersonaje(posPiccolo);
		for(int i = 0; i < 15; i++)
			majinboo.aumentarKi();
		for(int i = 0; i < 15; i++)
			piccolo.aumentarKi();		
		majinboo.ataqueEspecial(piccolo);
		for(int i = 0; i < 15; i++)
			piccolo.aumentarKi();
		piccolo.cambiarFase();		
	}

	// Item 9
	@Test
	public void gokuAumentaSuPoderDePeleaCuandoTieneMenosDe30PorcientoDeVida(){
		Tablero tablero=new Tablero(ANCHO, ALTO);
		Personaje goku= new Goku();
		Personaje cell= new Cell();
		Posicion posGoku=new Posicion(2,2);
		Posicion posCell=new Posicion(2,3);
		goku.setTablero(tablero);
		cell.setTablero(tablero);
		goku.ubicarPersonaje(posGoku);
		cell.ubicarPersonaje(posCell);
		goku.disminuirVida(490);
		goku.ataqueBasico(cell);
		assertEquals(cell.getVidaInicial()-24, cell.getVida(),0);
	}
}

