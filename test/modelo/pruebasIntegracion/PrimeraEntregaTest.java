package modelo.pruebasIntegracion;


import static org.junit.Assert.*;
import modelo.excepciones.*;
import org.junit.Test;
import modelo.*;
import modelo.personaje.*;
import modelo.tablero.Posicion;
import modelo.tablero.Tablero;


public class PrimeraEntregaTest {
	
	private static final int ANCHO=7;	
	private static final int ALTO=7;	

	// Item 1
	@Test
	public void ubicarGoku(){
		Tablero tablero=new Tablero(ANCHO, ALTO);
		Personaje goku= new Goku();
		Posicion pos=new Posicion(5,2);
		goku.setTablero(tablero);
		goku.ubicarPersonaje(pos);
		assertEquals(goku, tablero.getPersonajeCelda(pos));
	}
	@Test
	public void moverGoku2CasillerosEnModoNormalEsValido(){
		Tablero tablero=new Tablero(ANCHO, ALTO);
		Personaje goku= new Goku();
		Posicion pos=new Posicion(0,0);
		goku.setTablero(tablero);
		goku.ubicarPersonaje(pos);
		Posicion posFinal=new Posicion(2,1);
		goku.mover(posFinal);
		assertEquals(goku, tablero.getPersonajeCelda(posFinal));
		assertEquals(null, tablero.getPersonajeCelda(pos));
	}
	@Test(expected=RangoInvalido.class)
	public void moverGoku3CasillerosEnModoNormalNoTieneEfecto(){
		Tablero tablero=new Tablero(ANCHO, ALTO);
		Personaje goku= new Goku();
		Posicion pos=new Posicion(2,5);
		goku.setTablero(tablero);
		goku.ubicarPersonaje(pos);
		Posicion posFinal=new Posicion(2,2);
		goku.mover(posFinal);
	}
	// Item 2
	@Test
	public void ubicarUnPersonajeAUnCasilleroOcupadoNoTieneEfecto(){
		Tablero tablero=new Tablero(ANCHO, ALTO);
		Personaje goku= new Goku();
		Personaje gohan= new Gohan();
		Posicion pos=new Posicion(2,6);
		goku.setTablero(tablero);
		gohan.setTablero(tablero);
		goku.ubicarPersonaje(pos);
		gohan.ubicarPersonaje(pos);
		assertEquals(goku, tablero.getPersonajeCelda(pos));
	}
	@Test(expected=RangoInvalido.class)
	public void moverUnPersonajeAUnCasilleroOcupadoNoTieneEfecto(){
		Tablero tablero=new Tablero(ANCHO, ALTO);
		Personaje goku= new Goku();
		Posicion posGoku=new Posicion(2,6);
		goku.setTablero(tablero);
		goku.ubicarPersonaje(posGoku);
		Personaje gohan=new Gohan();
		Posicion posGohan=new Posicion(2,5);
		gohan.setTablero(tablero);
		gohan.ubicarPersonaje(posGohan);
		goku.mover(posGohan);
	}
	// Item 3
	@Test(expected=RangoInvalido.class)
	public void moverUnPersonajeConSuCaminoObstruidoNoTieneEfecto(){
		Tablero tablero=new Tablero(ANCHO, ALTO);
		Personaje goku= new Goku();
		Posicion posGoku=new Posicion(0,0);
		goku.setTablero(tablero);
		goku.ubicarPersonaje(posGoku);
		Personaje gohan=new Gohan();
		Posicion posGohan=new Posicion(0,1);
		gohan.setTablero(tablero);
		gohan.ubicarPersonaje(posGohan);
		Posicion posFinal=new Posicion(0,3);
		goku.mover(posFinal);
	}
	// Item 4 ya probado en test/modelo.personaje -> PruebasTransformacion.java
	
	// Item 5
	@Test
	public void moverGoku3CasillerosEnModoKaioKenEsValido(){
		Tablero tablero=new Tablero(ANCHO, ALTO);
		Personaje goku= new Goku();
		Posicion pos=new Posicion(0,0);
		goku.setTablero(tablero);
		goku.ubicarPersonaje(pos);
		for(int i = 0; i < 5; i++) {
			goku.aumentarKi();
		}
		goku.cambiarFase();
		Posicion posFinal=new Posicion(3,3);
		goku.mover(posFinal);
		assertEquals("Kaioken", goku.getNombreFaseActual());
		assertEquals(goku, tablero.getPersonajeCelda(posFinal));
		assertEquals(null, tablero.getPersonajeCelda(pos));
	}
	@Test(expected=RangoInvalido.class)
	public void moverGoku4CasillerosEnModoKaioKenNoTieneEfecto(){
		Tablero tablero=new Tablero(ANCHO, ALTO);
		Personaje goku= new Goku();
		Posicion pos=new Posicion(2,6);
		goku.setTablero(tablero);
		goku.ubicarPersonaje(pos);
		for(int i = 0; i < 4; i++) {
			goku.aumentarKi();
		}
		goku.cambiarFase();
		Posicion posFinal=new Posicion(2,2);
		goku.mover(posFinal);
	}
	// Item 6
	@Test
	public void alCrearUnJuegoSeUbicanLosPersonajesEnLosExtremos(){
		Juego juego=new Juego();
		Tablero tablero=juego.getTablero();
		int ancho = tablero.getAncho();
		int alto = tablero.getAlto();
		Posicion posPersonaje1=new Posicion(0,0);
		Posicion posPersonaje2=new Posicion(1,0);
		Posicion posPersonaje3=new Posicion(2,0);
		Posicion posPersonaje4=new Posicion(ancho-3,alto-1);
		Posicion posPersonaje5=new Posicion(ancho-2,alto-1);
		Posicion posPersonaje6=new Posicion(ancho-1,alto-1);
		assertNotNull(tablero.getPersonajeCelda(posPersonaje1));
		assertNotNull(tablero.getPersonajeCelda(posPersonaje2));
		assertNotNull(tablero.getPersonajeCelda(posPersonaje3));
		assertNotNull(tablero.getPersonajeCelda(posPersonaje4));
		assertNotNull(tablero.getPersonajeCelda(posPersonaje5));
		assertNotNull(tablero.getPersonajeCelda(posPersonaje6));
	}
	// Item 7
	@Test
	public void gokuAtacaACellA2CasillerosEnModoNormalEsValido(){
		Tablero tablero=new Tablero(ANCHO, ALTO);
		Personaje goku= new Goku();
		Posicion posGoku=new Posicion(0,0);
		goku.setTablero(tablero);
		goku.ubicarPersonaje(posGoku);
		Personaje cell=new Cell();
		Posicion posCell=new Posicion(0,2);
		cell.setTablero(tablero);
		cell.ubicarPersonaje(posCell);
		goku.ataqueBasico(cell);
		assertEquals(480,(int)cell.getVida());
	}
	@Test(expected=RangoInvalido.class)
	public void gokuAtacaACellA3CasillerosEnModoNormalNoTieneEfecto(){
		Tablero tablero=new Tablero(ANCHO, ALTO);
		Personaje goku= new Goku();
		Posicion posGoku=new Posicion(0,0);
		goku.setTablero(tablero);
		goku.ubicarPersonaje(posGoku);
		Personaje cell=new Cell();
		Posicion posCell=new Posicion(0,3);
		cell.setTablero(tablero);
		cell.ubicarPersonaje(posCell);
		goku.ataqueBasico(cell);
	}

}
