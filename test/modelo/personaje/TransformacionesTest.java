package modelo.personaje;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import modelo.excepciones.KiInsuficiente;
import modelo.excepciones.RangoInvalido;

public class TransformacionesTest {

	// Goku
	@Test
	public void alCrearAGokuSuFaseEsNormal() {
		Personaje goku = new Goku();
		assertEquals("Normal", goku.getNombreFaseActual());
	}
	
	@Test
	public void alTener20UnidadesDeKiGokuPuedePasarDeNormalAKaioKen() {
		Personaje goku = new Goku();
		
		for(int i = 0; i < 4; i++) {
			goku.aumentarKi();
		}
		
		goku.cambiarFase();
		assertEquals("Kaioken", goku.getNombreFaseActual());
	}
	
	@Test
	public void alCambiarGokuAKaioKenElKiDisminuyeEn20Unidades() {
		Personaje goku = new Goku();
		
		for(int i = 0; i < 4; i++) {
			goku.aumentarKi();
		}
		
		goku.cambiarFase();
		assertEquals(0, goku.getKi());
	}
	
	@Test
	public void alNoTener20UnidadesDeKiGokuNoPuedePasarDeNormalAKaioKen() {
		Personaje goku = new Goku();
		try {
			goku.cambiarFase();
		}
		
		catch (KiInsuficiente e) {
			assertEquals("Normal", goku.getNombreFaseActual());
		}
	}
	
	@Test
	public void alTener70UnidadesDeKiGokuPuedePasarDeNormalAKaioKenYDeKaioKenASuperSayajin() {
		Personaje goku = new Goku();
		
		for(int i = 0; i < 14; i++) {
			goku.aumentarKi();
		}
		
		goku.cambiarFase();
		goku.cambiarFase();
		assertEquals("SS", goku.getNombreFaseActual());
	}
	
	@Test
	public void alCambiarGokuASuperSayajinElKiDisminuye50Unidades() {
		Personaje goku = new Goku();
		
		for(int i = 0; i < 14; i++) {
			goku.aumentarKi();
		}
		goku.cambiarFase();
		goku.cambiarFase();
		assertEquals(0, goku.getKi());
	}
	
	@Test(expected=KiInsuficiente.class)
	public void alNoTener50UnidadesDeKiGokuNoPuedePasarDeKaioKenASuperSayajin() {
		Personaje goku = new Goku();
		
		for(int i = 0; i < 4; i++)
			goku.aumentarKi();
		goku.cambiarFase();
		goku.cambiarFase();
	}

	// Gohan
	@Test
	public void alCrearAGohanSuFaseEsNormal() {
		Personaje gohan = new Gohan();
		assertEquals("Normal", gohan.getNombreFaseActual());
	}
	
	// Piccolo
	@Test
	public void alCrearAPiccoloSuFaseEsNormal() {
		Personaje piccolo = new Piccolo();
		assertEquals("Normal", piccolo.getNombreFaseActual());
	}
	
	// Cell
	@Test
	public void alCrearACellSuFaseEsNormal() {
		Personaje cell = new Cell();
		assertEquals("Normal", cell.getNombreFaseActual());
	}
	
	// Freezer
	@Test
	public void alCrearAFreezerSuFaseEsNormal() {
		Personaje freezer = new Freezer();
		assertEquals("Normal", freezer.getNombreFaseActual());
	}
	
	@Test
	public void alTener20UnidadesDeKiFreezerPuedePasarDeNormalASegundaForma() {
		Personaje freezer = new Freezer();
		
		for(int i = 0; i < 4; i++) {
			freezer.aumentarKi();
		}
		
		freezer.cambiarFase();
		assertEquals("SegundaForma", freezer.getNombreFaseActual());
	}
	
	@Test
	public void alCambiarFreezerASegundaFormaElKiDisminuyeEn20Unidades() {
		Personaje freezer = new Freezer();
		
		for(int i = 0; i < 4; i++) {
			freezer.aumentarKi();
		}
		
		freezer.cambiarFase();
		assertEquals(0, freezer.getKi());
	}
	
	@Test
	public void alNoTener20UnidadesDeKiFreezerNoPuedePasarDeNormalASegundaForma() {
		Personaje freezer = new Freezer();
		
		try {
			freezer.cambiarFase();
		}
		
		catch (KiInsuficiente e) {
			assertEquals("Normal", freezer.getNombreFaseActual());
		}
	}
	
	@Test
	public void alTener70UnidadesDeKiFreezerPuedePasarDeNormalASegundaFormaYDeSegundaFormaADefinitivo() {
		Personaje freezer = new Freezer();
		
		for(int i = 0; i < 14; i++) {
			freezer.aumentarKi();
		}
		
		freezer.cambiarFase();
		freezer.cambiarFase();
		assertEquals("Definitivo", freezer.getNombreFaseActual());
	}
	
	@Test
	public void alCambiarFreezerADefinitivoElKiDisminuye50Unidades() {
		Personaje freezer = new Freezer();
		
		for(int i = 0; i < 14; i++) {
			freezer.aumentarKi();
		}
		
		freezer.cambiarFase();
		freezer.cambiarFase();
		assertEquals(0, freezer.getKi());
	}
	
	@Test(expected=KiInsuficiente.class)
	public void alNoTener50UnidadesDeKiFreezerNoPuedePasarDeSegundaFormaADefinitivo() {
		Personaje freezer = new Freezer();
		
		for(int i = 0; i < 4; i++) {
			freezer.aumentarKi();
		}
		freezer.cambiarFase();
		freezer.cambiarFase();
	}
	
	// Majin Boo
	@Test
	public void alCrearAMajinBooSuFaseEsNormal() {
		Personaje majinboo = new MajinBoo();
		assertEquals("Normal", majinboo.getNombreFaseActual());
	}

	@Test
	public void alTener20UnidadesDeKiMajinBooPuedePasarDeNormalABooMalo() {
		Personaje majinboo = new MajinBoo();
		
		for(int i = 0; i < 4; i++) {
			majinboo.aumentarKi();
		}
		
		majinboo.cambiarFase();
		assertEquals("Malo", majinboo.getNombreFaseActual());
	}
	
	@Test
	public void alCambiarMajinBooABooMaloElKiDisminuyeEn20Unidades() {
		Personaje majinboo = new MajinBoo();
		
		for(int i = 0; i < 4; i++) {
			majinboo.aumentarKi();
		}
		
		majinboo.cambiarFase();
		assertEquals(0, majinboo.getKi());
	}
	
	@Test
	public void alNoTener20UnidadesDeKiMajinBooNoPuedePasarDeNormalABooMalo() {
		Personaje majinboo = new MajinBoo();
		try {
			majinboo.cambiarFase();
		}
		
		catch (KiInsuficiente e) {
			assertEquals("Normal", majinboo.getNombreFaseActual());
		}
	}
	
	@Test
	public void alTener70UnidadesDeKiMajinBooPuedePasarDeNormalABooMaloYDeBooMaloABooOriginal() {
		Personaje majinboo = new MajinBoo();
		
		for(int i = 0; i < 14; i++) {
			majinboo.aumentarKi();
		}
		
		majinboo.cambiarFase();
		majinboo.cambiarFase();
		assertEquals("Original", majinboo.getNombreFaseActual());
	}
	
	@Test
	public void alCambiarMajinBooABooOriginalElKiDisminuye50Unidades() {
		Personaje majinboo = new MajinBoo();
		
		for(int i = 0; i < 14; i++) {
			majinboo.aumentarKi();
		}
		
		majinboo.cambiarFase();
		majinboo.cambiarFase();
		assertEquals(0, majinboo.getKi());
	}
	
	@Test(expected=KiInsuficiente.class)
	public void alNoTener50UnidadesDeKiMajinBooNoPuedePasarDeBooMaloABooOriginal() {
		Personaje majinboo = new MajinBoo();
		
		for(int i = 0; i < 4; i++)
			majinboo.aumentarKi();
		majinboo.cambiarFase();
		majinboo.cambiarFase();
	}
}
