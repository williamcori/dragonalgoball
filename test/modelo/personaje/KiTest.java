package modelo.personaje;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class KiTest {
	
	@Test
	public void alCrearAGokuSuKiEsCero() {
		Personaje goku = new Goku();
		assertEquals(0, goku.getKi());
	}
	
	@Test
	public void alAumentarCincoVecesElKiDeGokuEs25() {
		Personaje goku = new Goku();
		for(int i = 0; i < 5; i++) {
			goku.aumentarKi();
		}
		assertEquals(25, goku.getKi());
	}
	
	@Test
	public void alCrearAGohanSuKiEsCero() {
		Personaje gohan = new Gohan();
		assertEquals(0, gohan.getKi());
	}
	
	@Test
	public void alAumentarUnaVezElKiDeGohanEs5() {
		Personaje gohan = new Gohan();
		gohan.aumentarKi();
		assertEquals(5, gohan.getKi());
	}
	
	@Test
	public void alCrearAPiccoloSuKiEsCero() {
		Personaje piccolo = new Piccolo();
		assertEquals(0, piccolo.getKi());
	}
	
	@Test
	public void alAumentar20VecesElKiDePiccoloEs100() {
		Personaje piccolo = new Piccolo();
		for(int i = 0; i < 20; i++) {
			piccolo.aumentarKi();
		}
		assertEquals(100, piccolo.getKi());
	}
	
	@Test
	public void alCrearACellSuKiEsCero() {
		Personaje cell = new Cell();
		assertEquals(0, cell.getKi());
	}
	
	@Test
	public void alAumentarDiezVecesElKiDeCellEs50() {
		Personaje cell = new Cell();
		for(int i = 0; i < 10; i++) {
			cell.aumentarKi();
		}
		assertEquals(50, cell.getKi());
	}
	
	@Test
	public void alCrearAFreezerSuKiEsCero() {
		Personaje freezer = new Freezer();
		assertEquals(0, freezer.getKi());
	}
	
	@Test
	public void alAumentarCincoVecesElKiDeFreezerEs25() {
		Personaje freezer = new Freezer();
		for(int i = 0; i < 5; i++) {
			freezer.aumentarKi();
		}
		assertEquals(25, freezer.getKi());
	}
	
	@Test
	public void alCrearAMajinBooSuKiEsCero() {
		Personaje majinboo = new MajinBoo();
		assertEquals(0, majinboo.getKi());
	}
	
	@Test
	public void alAumentar17VecesElKiDeMajinBooEs85() {
		Personaje majinboo = new MajinBoo();
		for(int i = 0; i < 17; i++) {
			majinboo.aumentarKi();
		}
		assertEquals(85, majinboo.getKi());
	}
}
