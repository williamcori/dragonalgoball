package modelo.personaje;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class NombreTest {

	@Test
	public void ElNombreDeGokuEsElCorrecto() {
		Personaje goku = new Goku();
		assertEquals("Goku", goku.getNombre());
	}
	
	@Test
	public void ElNombreDeGohanEsElCorrecto() {
		Personaje gohan = new Gohan();
		assertEquals("Gohan", gohan.getNombre());
	}
	
	@Test
	public void ElNombreDePiccoloEsElCorrecto() {
		Personaje piccolo = new Piccolo();
		assertEquals("Piccolo", piccolo.getNombre());
	}
	
	@Test
	public void ElNombreDeCellEsElCorrecto() {
		Personaje cell = new Cell();
		assertEquals("Cell", cell.getNombre());
	}
	
	@Test
	public void ElNombreDeFreezerEsElCorrecto() {
		Personaje freezer = new Freezer();
		assertEquals("Freezer", freezer.getNombre());
	}
	
	@Test
	public void ElNombreDeMajinBooEsElCorrecto() {
		Personaje majinboo = new MajinBoo();
		assertEquals("MajinBoo", majinboo.getNombre());
	}
}
