package modelo.tablero;

import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import modelo.personaje.Goku;

public class TableroTest {
	
	@Test
	public void tableroInicialTieneTodasLasCeldasVacias(){
		Tablero tablero = new Tablero(8, 10);
		
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 10; j++){
				Posicion pos = new Posicion(i, j);
				assertTrue("La celda " + pos.toString() + " deberia estar vacia",
							tablero.celdaVacia(pos));				
			}
		}
	}
	
	@Test
	public void posicionValidaConPosicionXnegativaYPositiva(){
		Tablero tablero = new Tablero(8, 10);
		
		Posicion pos = new Posicion(-1, 2);
		
		assertFalse("La posicion " + pos.toString() + "no pertenece al tablero",
					tablero.posicionValida(pos));
	}
	
	@Test
	public void posicionValidaConPosicionXPositivaYNegativa(){
		Tablero tablero = new Tablero(8, 10);
		
		Posicion pos = new Posicion(1, -2);
		
		assertFalse("La posicion " + pos.toString() + "no pertenece al tablero",
					tablero.posicionValida(pos));
	}
	
	@Test
	public void posicionValidaConPosicionXnegativaYNegativa(){
		Tablero tablero = new Tablero(8, 10);
		
		Posicion pos = new Posicion(-1, -2);
		
		assertFalse("La posicion " + pos.toString() + "no pertenece al tablero",
					tablero.posicionValida(pos));
	}
	
	@Test
	public void posicionValidaConPosicionXdentroDelLimiteYFueraDelLimite(){
		Tablero tablero = new Tablero(8, 10);
		
		Posicion pos = new Posicion(7, 11);
		
		assertFalse("La posicion " + pos.toString() + "no pertenece al tablero",
					tablero.posicionValida(pos));
	}
	
	@Test
	public void posicionValidaConPosicionXFueraDelLimiteYdentroDelLimite(){
		Tablero tablero = new Tablero(8, 10);
		
		Posicion pos = new Posicion(9, 9);
		
		assertFalse("La posicion " + pos.toString() + "no pertenece al tablero",
					tablero.posicionValida(pos));
	}
	
	@Test
	public void posicionValidaConPosicionDentroDeLosLimitesDelTablero(){
		Tablero tablero = new Tablero(8, 10);
		
		Posicion pos = new Posicion(7, 9);
		
		assertTrue("La posicion " + pos.toString() + "pertenece al tablero",
					tablero.posicionValida(pos));
		
		pos = new Posicion(0, 0);
		
		assertTrue("La posicion " + pos.toString() + "pertenece al tablero",
				tablero.posicionValida(pos));	
	}
	
	@Test
	public void movimientoValidoSinBloqueosConDistanciaIgualACantidadMovimientos(){
		Tablero tablero = new Tablero(8,10);
		Posicion posInicial = new Posicion(1,1);
		Posicion posFinal = new Posicion(2, 5);
		
		assertTrue("El movimiento deberia ser valido", 
				tablero.distanciaValida(posInicial, posFinal, 4));
	}
	
	@Test
	public void movimientoValidoSinBloqueosConDistanciaMenorACantidadMovimientos(){
		Tablero tablero = new Tablero(8,10);
		Posicion posInicial = new Posicion(1,2);
		Posicion posFinal = new Posicion(6, 7);
		
		assertTrue("El movimiento deberia ser valido", 
				tablero.distanciaValida(posInicial, posFinal, 9));
	}
	
	@Test
	public void movimientoValidoSinBloqueosConDistanciaMayorACantidadMovimientos(){
		Tablero tablero = new Tablero(8,10);
		Posicion posInicial = new Posicion(4,4);
		Posicion posFinal = new Posicion(1, 1);
		
		assertFalse("No deberia alcanzar la cantidad de movimientos para llegar a destino", 
				tablero.distanciaValida(posInicial, posFinal, 2));
	}
	
	@Test
	public void movimientoValidoCon1BloqueoConDistanciaIgualACantidadMovimientos(){
		Tablero tablero = new Tablero(8,10);
		Posicion posInicial = new Posicion(1,1);
		Posicion posFinal = new Posicion(3, 3);
		Goku gokuBloqueo = new Goku();
		Posicion posGokuBloqueo = new Posicion(2, 2);
		
		tablero.setPersonajeCelda(posGokuBloqueo, gokuBloqueo);
		
		assertFalse("Deberia ser invalido porque requiere mas movimientos por el bloqueo", 
				tablero.distanciaValida(posInicial, posFinal, 2));
	}
	
	@Test
	public void movimientoValidoCon1BloqueoConDistanciaMenorACantidadMovimientos(){
		Tablero tablero = new Tablero(8,10);
		Posicion posInicial = new Posicion(3,3);
		Posicion posFinal = new Posicion(1, 5);
		Goku gokuBloqueo = new Goku();
		Posicion posGokuBloqueo = new Posicion(2, 4);
		
		tablero.setPersonajeCelda(posGokuBloqueo, gokuBloqueo);
		
		assertTrue("Deberia ser valido porque puede rodear el bloqueo", 
				tablero.distanciaValida(posInicial, posFinal, 3));
	}
	
	@Test
	public void movimientoValidoCon3BloqueosConDistanciaMenorACantidadMovimientos(){
		Tablero tablero = new Tablero(8,10);
		Posicion posInicial = new Posicion(3,3);
		Posicion posFinal = new Posicion(3, 6);
		Goku gokuBloqueo1 = new Goku();
		Posicion posGokuBloqueo1 = new Posicion(3, 5);
		Goku gokuBloqueo2 = new Goku();
		Posicion posGokuBloqueo2 = new Posicion(2, 5);
		Goku gokuBloqueo3 = new Goku();
		Posicion posGokuBloqueo3 = new Posicion(4, 5);
		
		tablero.setPersonajeCelda(posGokuBloqueo1, gokuBloqueo1);
		tablero.setPersonajeCelda(posGokuBloqueo2, gokuBloqueo2);
		tablero.setPersonajeCelda(posGokuBloqueo3, gokuBloqueo3);
		
		assertTrue("Deberia ser valido porque puede rodear el bloqueo", 
				tablero.distanciaValida(posInicial, posFinal, 4));
	}
	
	@Test
	public void movimientoValidoCon3BloqueosConDistanciaIgualACantidadMovimientos(){
		Tablero tablero = new Tablero(8,10);
		Posicion posInicial = new Posicion(3,3);
		Posicion posFinal = new Posicion(3, 6);
		Goku gokuBloqueo1 = new Goku();
		Posicion posGokuBloqueo1 = new Posicion(3, 5);
		Goku gokuBloqueo2 = new Goku();
		Posicion posGokuBloqueo2 = new Posicion(2, 5);
		Goku gokuBloqueo3 = new Goku();
		Posicion posGokuBloqueo3 = new Posicion(4, 5);
		
		tablero.setPersonajeCelda(posGokuBloqueo1, gokuBloqueo1);
		tablero.setPersonajeCelda(posGokuBloqueo2, gokuBloqueo2);
		tablero.setPersonajeCelda(posGokuBloqueo3, gokuBloqueo3);
		
		assertFalse("Deberia ser invalido porque no le alcanzan los movimientos para rodear el bloqueo", 
				tablero.distanciaValida(posInicial, posFinal, 3));
	}
}
