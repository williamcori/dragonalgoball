package modelo.tablero;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;





public class PosicionTest {
	
	
	@Test
	public void esNegativaConCoordenadasXNegativaYPositiva(){
		Posicion posicion = new Posicion(-1, 1);
		assertTrue("Deberia ser negativa si X es negativa", posicion.esNegativa());		
	}
	
	@Test
	public void esNegativaConCoordenadasXPositivaYNegativa(){
		Posicion posicion = new Posicion(1, -1);
		assertTrue("Deberia ser negativa si Y es negativa", posicion.esNegativa());
	}
	
	@Test
	public void esNegativaConCoordenadaXNegativaYNegativa(){
		Posicion posicion = new Posicion(-1, -1);
		assertTrue("Deberia ser negativa si X es negativa e Y tambien", 
					posicion.esNegativa());
	}
	
	@Test
	public void esNegativaConCoordenadaXPositivaYPositiva(){
		Posicion posicion = new Posicion(0, 0);
		assertFalse("Deberia No ser negativa el origen no es negativo", 
					posicion.esNegativa());
	}
	
	
	
	@Test
	public void distanciaEntreDosPosicionesPositivas(){
		Posicion posicion = new Posicion(5,10);
		Posicion otraPosicion = new Posicion(10,12);
		
		assertEquals("La distancia deberia ser 5",5,posicion.distancia(otraPosicion));
	}
	
	@Test
	public void distanciaEntrePosicionPositivaYNegativa(){
		Posicion posicion = new Posicion(1,2);
		Posicion otraPosicion = new Posicion(-3,-4);
		
		assertEquals("La distancia deberia ser 6",6,posicion.distancia(otraPosicion));
	}
	
	@Test
	public void distanciaEntreDosPosicionesNegativas(){
		Posicion posicion = new Posicion(1,-2);
		Posicion otraPosicion = new Posicion(-3,2);
		
		assertEquals("La distancia deberia ser 4",4,posicion.distancia(otraPosicion));
	}

	
	@Test
	public void menorQueConUnaPosicionMenorQueLaOtra(){
		Posicion posicion = new Posicion(1, 1);
		Posicion otraPosicion = new Posicion(5, 3);
		
		assertTrue("La primera deberia ser menor que la segunda",
					posicion.menorQue(otraPosicion));
	}
	
	@Test
	public void menorQueConPosicionesDondeNingunaEsMenor(){
		Posicion posicion = new Posicion(3, 3);
		Posicion otraPosicion = new Posicion(1, 5);
		
		assertFalse("Deberia ser falso porque la primera no es menor que la segunda",
					posicion.menorQue(otraPosicion));
		assertFalse("Deberia ser falso porque la segunda no es menor que la primera",
				otraPosicion.menorQue(posicion));
	}
	
	
	@Test
	public void equalsConPosicionesIgualesEnXDistintasEnY(){
		Posicion posicion = new Posicion(3, 3);
		Posicion otraPosicion = new Posicion(3, 5);
		
		assertFalse("Deberia ser falso porque es distinta en Y", posicion.equals(otraPosicion));
	}
	
	@Test
	public void equalsConPosicionesIgualesEnYDistintasEnX(){
		Posicion posicion = new Posicion(3, 3);
		Posicion otraPosicion = new Posicion(5, 3);
		
		assertFalse("Deberia ser falso porque es distinta en X", posicion.equals(otraPosicion));
	}
	
	@Test
	public void equalsConPosicioneDistintasEnAmbasComponentes(){
		Posicion posicion = new Posicion(1, 3);
		Posicion otraPosicion = new Posicion(3, 5);
		
		assertFalse("Deberia ser falso porque es distinta en X e Y", posicion.equals(otraPosicion));
	}
	
	@Test
	public void equalsConPosicionesIdenticasEnAmbasComponentes(){
		Posicion posicion = new Posicion(3, 3);
		Posicion otraPosicion = new Posicion(3, 3);
		
		assertTrue("Deberia ser verdadero porque tienen mismas componentes", posicion.equals(otraPosicion));
	}

	
	@Test
	public void posicionesColindantesConPosicionesAdistancia1(){
		Posicion posicion = new Posicion(3, 3);
		
		Posicion[] posicionesConlindantes = posicion.posicionesColindantes();
		List<Posicion> listaPosColindantes = new ArrayList<Posicion>();
		
		for(Posicion p : posicionesConlindantes)
			listaPosColindantes.add(p);
		
		assertTrue("El (3,4) deberia pertenecer", 
					listaPosColindantes.contains(new Posicion(3,4)));
		
		assertTrue("El (3,2) deberia pertenecer", 
				listaPosColindantes.contains(new Posicion(3,2)));
		
		assertTrue("El (4,2) deberia pertenecer", 
				listaPosColindantes.contains(new Posicion(4,2)));
		
		assertTrue("El (4,3) deberia pertenecer", 
				listaPosColindantes.contains(new Posicion(4,3)));
		
		assertTrue("El (4,4) deberia pertenecer", 
				listaPosColindantes.contains(new Posicion(4,4)));
		
		assertTrue("El (2,2) deberia pertenecer", 
				listaPosColindantes.contains(new Posicion(2,2)));
		
		assertTrue("El (2,3) deberia pertenecer", 
				listaPosColindantes.contains(new Posicion(2,3)));

		assertTrue("El (2,4) deberia pertenecer", 
				listaPosColindantes.contains(new Posicion(2,4)));
	}
	
	@Test
	public void posicionesColindantesConPosicionesAdistanciaMayorA1(){
		Posicion posicion = new Posicion(3, 3);
		
		Posicion[] posicionesConlindantes = posicion.posicionesColindantes();
		List<Posicion> listaPosColindantes = new ArrayList<Posicion>();
		
		for(Posicion p : posicionesConlindantes)
			listaPosColindantes.add(p);
		
		assertFalse("El (3,5) no deberia pertenecer", 
				listaPosColindantes.contains(new Posicion(3,5)));
		
		assertFalse("El (5,3) no deberia pertenecer", 
				listaPosColindantes.contains(new Posicion(5,3)));
		
		assertFalse("El (1,3) no deberia pertenecer", 
				listaPosColindantes.contains(new Posicion(1,3)));
		
		assertFalse("El (3,1) no deberia pertenecer", 
				listaPosColindantes.contains(new Posicion(3,1)));
	}
}
