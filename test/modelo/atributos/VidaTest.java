package modelo.atributos;

import org.junit.Assert;
import org.junit.Test;

import modelo.atributos.vida.Vida;
import modelo.excepciones.ValorInvalido;

public class VidaTest {
	@Test
	public void crearVidaYVerificarValor(){
		Vida vida = new Vida(500);
		Assert.assertEquals("La prueba fallo el valor es distinto de 500",500, vida.getVida(),0);
	}
	
	@Test
	public void hacerDamageAVidaYVerificarValor(){
		Vida vida = new Vida(500);
		vida.disminuirVida(200);
		Assert.assertEquals("La prueba fallo el valor es distinto de 300",300, vida.getVida(),0);
	}
	
	@Test (expected = ValorInvalido.class)
	public void hacerDamageNegativoLanzaValorInvalido(){
		Vida vida = new Vida(500);
		vida.disminuirVida(-200);
	}
}
