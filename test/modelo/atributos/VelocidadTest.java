package modelo.atributos;

import org.junit.Assert;
import org.junit.Test;

import modelo.atributos.velocidad.Velocidad;
import modelo.excepciones.VelocidadInvalida;

public class VelocidadTest {
	Velocidad velocidad;
	@Test
	public void crearVelocidadYverificarValor(){
		velocidad = new Velocidad(4);
		Assert.assertEquals("La prueba NO paso: el valor no es con el que se creo",4, velocidad.getVelocidad());
	}
	
	@Test (expected = VelocidadInvalida.class)
	public void crearVelocidadConValorMenorACeroLanzaExcepcion(){
		@SuppressWarnings("unused")
		Velocidad velocidad = new Velocidad(-7);
	}
	
	@Test (expected = VelocidadInvalida.class)
	public void crearVelocidadConValorIgualACeroLanzaExcepcion(){
		@SuppressWarnings("unused")
		Velocidad velocidad = new Velocidad(0);
	}
	
	@Test
	public void activarElDobleDeVelocidadYVerificarValor(){
		Velocidad velocidad = new Velocidad(4);
		velocidad.activarDobleVelocidad();
		Assert.assertEquals("La prueba NO paso: El valor esperado era el doble de 4 = 8",8, velocidad.getVelocidad());
	}
	
	@Test
	public void activarElDobleDeVelocidadYDesactivarloYVerificarValor(){
		Velocidad velocidad = new Velocidad(4);
		velocidad.activarDobleVelocidad();
		velocidad.desactivarDobleVelocidad();
		Assert.assertEquals("La prueba NO: EL valor esperado era el valor inicial 4",4, velocidad.getVelocidad());
	}
}
