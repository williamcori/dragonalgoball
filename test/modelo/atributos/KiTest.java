package modelo.atributos;

import org.junit.Assert;
import org.junit.Test;

import modelo.atributos.ki.Ki;

public class KiTest {
	@Test
	public void alCrearKiElValorInicialEsCero() {
		Ki ki = new Ki();
		Assert.assertEquals("La prueba NO pasó: el ki inicial no es 0",0, ki.getKi());
	}
	
	@Test
	public void crearKiYAumentarloUnTurno(){
		Ki ki = new Ki();
		ki.aumentarKi();
		Assert.assertEquals("La prueba NO pasó: el Ki era 0 y debia aumentar a 5",5,ki.getKi());
	}
	
	@Test
	public void crearKiYAumentarloDosTurnos(){
		Ki ki = new Ki();
		ki.aumentarKi();
		ki.aumentarKi();
		Assert.assertEquals("La prueba NO pasó: el Ki era 0 y debia aumentar a 10",10,ki.getKi());
	}
}
